retrofix
========

RetroFix is a python library for reading and writing fixed-size field text file
and more specifically for managing files from Spanish banks and public
institutions, including:

- CSB 19, 32, 34 (several versions), 43, 57 & 58
- AEAT 111, 115, 130, 180, 182, 190, 303, 340, 347, 349 & 390

Installing
----------

See INSTALL

Package Contents
----------------

  doc/
      documentation in reStructuredText. To generate the HTML:

        make html

  retrofix/
      retrofix sources

Support
-------

License
-------

See LICENSE

Copyright
---------

See project files for the complete list of Copyright owners.
