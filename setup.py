#!/usr/bin/env python
# encoding: utf8
##############################################################################
#
#    Copyright (C) 2011-2013 NaN Projectes de Programari Lliure, S.L.
#                            http://www.NaN-tic.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


import os

from setuptools import find_packages, setup

from retrofix.version import LICENSE, PACKAGE, VERSION, WEBSITE

tests_require = ['pytest==5.3.1', 'pytest-checkipdb==0.3.5', 'pytest-sugar==0.9.2', 'pytest-black==0.3.7', 'parameterized==0.7.0']

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name=PACKAGE,
    version=VERSION,
    description='retrofix',
    long_description=read('README.md'),
    author='Txerpa',
    author_email='info@txerpa.com',
    url=WEBSITE,
    download_url='https://pypi.python.org/pypi/retrofix-txerpa/' + VERSION,
    packages=find_packages(),
    package_data={'retrofix.tests': ['c19.txt', 'c32.txt', 'c34.txt', 'c43.txt', 'c57.txt', 'c58.txt']},
    scripts=[],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries',
    ],
    license=LICENSE,
    install_requires=['python-stdnum == 1.16'],
    tests_require=tests_require,
    extras_require={'testing': tests_require},
    zip_safe=False,
    test_suite='tests',
    project_urls={
        'Documentation': 'https://bitbucket.org/txerpa/python-retrofix-git/',
        'Source': 'https://bitbucket.org/txerpa/python-retrofix-git/src/master/',
    },
)
