# -*- coding: utf-8 -*-


##############################################################################
#
#    Copyright (C) 2013 NaN Projectes de Programari Lliure, S.L.
#                            http://www.NaN-tic.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import sys
from distutils.util import strtobool

try:
    import cdecimal
    # Use cdecimal globally
    if 'decimal' not in sys.modules:
        sys.modules["decimal"] = cdecimal
except ImportError:
    pass
from decimal import Decimal
import re
from datetime import datetime

from stdnum.es.ccc import is_valid

from .formatting import format_string, format_number
from .exception import RetrofixException

__all__ = ['Field', 'Char', 'Const', 'Account', 'Number', 'Numeric', 'Integer',
           'Date', 'Selection', 'Boolean', 'SIGN_DEFAULT', 'SIGN_12', 'SIGN_N',
           'SIGN_N_BLANK', 'WITHOUT_SIGN', 'BOOLEAN_01', 'BOOLEAN_12', 'BOOLEAN_X',
           'BOOLEAN_W1']

"""
Field types used by aeat models and bank notebooks 
"""


class Field(object):
    def __init__(self):
        self._name = None
        self._size = None

    def set_from_file(self, value):
        assert len(value) == self._size, (f'Invalid length of field "{self._name}". '
                                          f'Expected "{self._size}" but got "{len(value)}".')
        return value

    def get_for_file(self, value):
        if value is None:
            value = ''
        return format_string(value, self._size)

    def get(self, value):
        return value

    def set(self, value):
        return value


class Char(Field):
    """
    Alfanumeric fields, Left just
    """
    def __init__(self):
        super(Char, self).__init__()

    def set_from_file(self, value):
        value = value.ljust(self._size)
        return super(Char, self).set_from_file(value)

    def set(self, value):
        if value is None:
            value = ''
        if len(value) > self._size:
            value = value[:self._size]
        return super(Char, self).set(value)


class Const(Char):
    """
    Constant fields
    """
    def __init__(self, const):
        super(Const, self).__init__()
        self._const = const

    def set_from_file(self, value):
        assert value == self._const, (f'Invalid value "{value}" in Const field'
                                      f' "{self._name}". Expected "{self._const}".')
        return super(Const, self).set_from_file(value)

    def get_for_file(self, value):
        return self._const

    def get(self, value):
        return self._const

    def set(self, value):
        assert value == self._const, (f'Invalid value "{value}" in Const '
                                      f'field "{self._name}". Expected "{self._const}".')
        return super(Const, self).set(value)


class Account(Char):
    def __init__(self):
        super(Account, self).__init__()

    def set_from_file(self, value):
        account = value.strip()
        if account and not is_valid(account):
            raise RetrofixException(f'Invalid bank account "{value}" in field "{self._name}"')
        return super(Account, self).set_from_file(value)

    def set(self, value):
        account = value
        if account:
            account = account.strip()
        if account and not is_valid(account):
            raise RetrofixException(f'Invalid bank account "{value}" in field "{self._name}"')
        return super(Account, self).set(value)


class Number(Char):
    """
    Numeric Text fields
    """
    def __init__(self, align='right'):
        super(Number, self).__init__()
        self._align = align

    def set_from_file(self, value):
        assert re.match('[0-9]*$', value), f'Non-number value "{value}" in field "{self._name}"'
        return super(Number, self).set_from_file(value)

    def set(self, value):
        if value is None:
            value = ''
        elif type(value) is not str:
            value = str(value)

        assert re.match('[0-9]*$', value), f'Non-number value "{value}" in field "{self._name}"'

        length = self._size - len(value)
        if self._align == 'right' and length:
            value = value.rjust(self._size, str('0'))
        return super(Number, self).set(value)


SIGN_DEFAULT = 'default'
SIGN_12 = 'sign_12'
SIGN_N = 'sgin_n'
SIGN_N_BLANK = 'sign_n_blank'
WITHOUT_SIGN = 'positive'


class Numeric(Field):
    def __init__(self, decimals=2, sign=SIGN_DEFAULT):
        """
        Numeric fields
        :param decimals: default value 2
        :param sign: default value 'SIGN_DEFAULT'
                Accept values:
                 'SIGN_DEFAULT'
                                + = ''
                                - = -
                 'SIGN_N': Numeric fields with SIGN_N accept number and character 'N'
                  + = ''
                  - = 'N'
        """
        super(Numeric, self).__init__()
        self._decimals = decimals
        self._sign = sign

    def get_sign(self, value):
        if self._sign == SIGN_DEFAULT:
            return '' if value >= Decimal(0) else '-'
        if self._sign == SIGN_12:
            return '2' if value >= Decimal(0) else '1'
        if self._sign == SIGN_N:
            return '' if value >= Decimal(0) else 'N'
        if self._sign == SIGN_N_BLANK:
            return ' ' if value >= Decimal(0) else 'N'
        if self._sign == WITHOUT_SIGN:
            return ''

    def set_from_file(self, value):
        super(Numeric, self).set_from_file(value)
        sign = 1
        if self._sign in (SIGN_12, SIGN_N):
            sign = -1 if value[0] in ('1', 'N') else 1
            value = value[1:]
        if not self._decimals:
            num = sign * Decimal(value)
        else:
            num = sign * Decimal('%s.%s' % (value[:-self._decimals],
                value[-self._decimals:]))
        return num

    def get_for_file(self, value):
        if value is None:
            value = Decimal('0')
        sign = self.get_sign(value)
        length = self._size - len(sign)
        assert length > 0, f'Number formatting error. Field size "{self._size}"' \
            f' but only "{length}" characters left for formatting field "{self._name}"'
        return sign + format_number(abs(value), length, self._decimals)

    def set(self, value):
        try:
            if not value:
                return Decimal(0)
            if type(value) is not Decimal:
                if type(value) is str:
                    value = value.replace(',', '.')
                else:
                    value = str(value)
                return Decimal(value)
            if self._sign == WITHOUT_SIGN:
                return abs(value)
            return value
        except (TypeError, ValueError):
            raise RetrofixException(f'Invalid value "{value}" for Numeric Field "{self._name}"')


class Integer(Numeric):
    def __init__(self):
        """
        Numeric fields without decimals
        """
        super(Integer, self).__init__(decimals=0)


class Date(Field):
    def __init__(self, pattern):
        super(Date, self).__init__()
        self._pattern = pattern

    def set_from_file(self, value):
        if value == '0' * len(value):
            return
        try:
            return datetime.strptime(value, self._pattern)
        except ValueError:
            raise RetrofixException(f'Invalid date value "{value}" does not '
                                    f'match pattern "{self._pattern}" in field "{self._name}"')

    def get_for_file(self, value):
        if value is None:
            res = ''
        else:
            res = datetime.strftime(value, self._pattern)
        return super(Date, self).get_for_file(res)

    def set(self, value):
        if value is not None:
            assert value, datetime
        return super(Date, self).set(value)


class Selection(Char):
    def __init__(self, selection):
        super(Selection, self).__init__()
        self._selection = selection
        self._keys = dict([(x[0], x[1]) for x in selection])
        self._values = dict([(x[1], x[0]) for x in selection])

    def get_for_file(self, value):
        return super(Selection, self).get_for_file(value)

    def set_from_file(self, value):
        assert value in self._keys, f'Value "{value}" not found in selection' \
            f' field "{self._name}". Expected one of: {self._keys}'
        return super(Selection, self).set_from_file(value)

    def set(self, value):
        assert value in self._values, f'Value "{value}" not found in selection' \
            f' field "{self._name}". Expected one of: {self._values}'
        value = self._values[value]
        return super(Selection, self).set(value)


BOOLEAN_01 = {
    True: '1',
    False: '0',
}

BOOLEAN_12 = {
    True: '1',
    False: '2',
}

BOOLEAN_X = {
    True: 'X',
    False: ' ',
}

BOOLEAN_W1 = {
    True: '1',
    False: ' ',
}


class Boolean(Field):
    def __init__(self, formatting=None):
        super(Boolean, self).__init__()
        if formatting is None:
            formatting = BOOLEAN_01
        self._formatting = formatting

    def set_from_file(self, value):
        for key, text in self._formatting.items():
            if value == text:
                return key
        raise RetrofixException(f'Invalid value "{value}" for boolean field "{self._name}"')

    def get_for_file(self, value):
        if value is None:
            value = False
        return self._formatting[value]

    def _value_to_bool(self, value):
        if value is None:
            return False
        if type(value) != bool:
            value = str(value).upper()
            if value == 'TRUE' or value == 'FALSE':
                return strtobool(value)
            else:
                assert value in self._formatting.values(), \
                    f'Invalid value "{value}" for boolean field "{self._name}".' \
                    f' You can use: {self._formatting.values()}'
                inverse_dict = dict(map(reversed, self._formatting.items()))
                return inverse_dict[value]

    def get(self, value):
        if type(value) != bool:
            value = self._value_to_bool(value)
        return value

    def set(self, value):
        if type(value) != bool:
            value = self._value_to_bool(value)
        return super(Boolean, self).set(value)
