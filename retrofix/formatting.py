# -*- coding: utf-8 -*-

##############################################################################
#
#    Copyright (C) 2013 NaN Projectes de Programari Lliure, S.L.
#                            http://www.NaN-tic.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import re, unicodedata
from decimal import Decimal


def clean_unsupported_chars(text):
    # Remove no-printable chars (All entries in the ASCII table below
    # code 32) since these chars modifies position of the data
    text_without_ctrl_chars = re.compile(r'[\x00-\x1F\x7F]').sub('', text)
    # Remove not supported ISO-8859-1 chars: !$%*+<-?\[-^`{-¿×ØÞßð÷øþ.
    not_supported_character_pattern = re.compile(
        r'[\x21\x24\x25\x2A\x2B\x3C-\x3F\x5B-\x5E\x60\x7B-\xBF\xC6\xD7'
        r'\xD8\xDE\xDF\xE6\xF0\xF7\xF8\xFE]'
    )
    text_without_no_supported_chars = not_supported_character_pattern.sub(
        '', text_without_ctrl_chars)
    # Translate not supported accented chars
    spec_chars_translation = str.maketrans('ÂÃÅÊÎÐÔÕÛÝâãåêîôõûýÿ',
                                           'AAAEIDOOUYaaaeioouyy')
    translated_text = text_without_no_supported_chars.translate(
        spec_chars_translation)
    # Silently remove all non iso-8859-1 chars
    encoding = 'iso-8859-1'
    # Debido a que la web de hacienda aún no tolera acentos y que no tenemos
    # manera agil de soportar las dos versiones por ahora removeremos los acentos siempre
    # return translated_text.encode(encoding, 'ignore').decode(encoding)
    return unicodedata.normalize('NFKD', translated_text).encode('iso-8859-1', 'ignore').decode()


def format_string(text, length, fill=' ', align='<'):
    if not text:
        return fill * length
    if isinstance(text, bytes):
        text = text.decode()
    if isinstance(text, str):
        text = clean_unsupported_chars(text)
    else:
        text = str(text)
    if len(text) > length:
        text = text[:length]
    text = '{0:{1}{2}{3}s}'.format(text, fill, align, length)
    assert len(text) == length, 'Formatted string must match the given length'
    return text.upper()


def format_number(number, size, decimals=0):
    assert number >= Decimal('0.0')
    length = size
    if decimals > 0:
        length += 1
    text = '{0:{1}{2}{3}.{4}f}'.format(number, '0', '>', length, decimals)
    text = text.replace('.', '')
    assert len(text) == size, f'Formatted number " {number}" must match the given length "{size}". Got: "{text}".'
    return text
