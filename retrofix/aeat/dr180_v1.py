# -*- coding: utf-8 -*-

from retrofix.fields import Char, Const, Number, Numeric, SIGN_N_BLANK, WITHOUT_SIGN, Integer

"""
Diseño de registro del modelo 180 (v 1)
-----------------------------------------------------------------------------------------------------
180 - Orden HAP/1732/2014, de 24 de septiembre
  
Retenciones e ingresos a cuenta. 
Rendimientos procedentes del arrendamiento de inmuebles urbanos. Resumen anual.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '180'

# Tipo 1: Registro del declarante.
RECORD_1 = (
    (1,   1,   'record_type',                         Const('1')),                   # TIPO DE REGISTRO
    (2,   3,   'model',                               Const(MODEL)),                 # MODELO DECLARACIÓN
    (5,   4,   'fiscalyear',                          Number),                       # EJERCICIO
    (9,   9,   'nif',                                 Char),                         # NIF DEL DECLARANTE
    (18,  40,  'presenter_name',                      Char),                         # APELLIDOS Y NOMBRE O RAZÓN SOCIAL DEL DECLARANTE
    (58,  1,   'support_type',                        Char),                         # TIPO DE SOPORTE
    # PERSONA CON QUIÉN RELACIONARSE                                                 
    (59,  9,   'contact_phone',                       Number),                       # TELÉFONO
    (68,  40,  'contact_name',                        Char),                         # APELLIDOS Y NOMBRE
                                                                                     
    (108, 13,  'declaration_number',                  Integer),                      # NUMERO DE IDENTIFICATIVO DE LA DECLARACION
    # DECLARACION COMPLEMENTARIA O SUSTITUTIVA                                       
    (121, 1,   'complementary',                       Char),                         # DECLARACIÓN COMPLEMENTARIA
    (122, 1,   'replacement',                         Char),                         # DECLARACIÓN SUSTITUTIVA
                                                                                     
    (123, 13,  'previous_declaration_receipt',        Number),                       # NUMERO IDENTIFICATIVO DE LA DECLARACIÓN ANTERIOR
    (136, 9,   'number_of_receivers',                 Integer),                      # NÚMERO TOTAL DE PERCEPTORES
    (145, 16,  'total_withholding_base',              Numeric(sign=SIGN_N_BLANK)),   # BASE RETENCIONES E INGRESOS A CUENTA
    (161, 15,  'total_withholding_tax',               Numeric),                      # RETENCIONES E INGRESOS A CUENTA
    (176, 61,  'reserved_aeat',                       Char),                         # BLANCOS
    (238, 263,  'electronic_stamp_aeat',              Char)                          # SELLO ELECTRONICO
)

# Tipo 2: Registro de perceptor.
RECORD_2 = (
    (1,   1,   'record_type',                         Const('2')),  # TIPO DE REGISTRO
    (2,   3,   'model',                               Const(MODEL)),  # MODELO DECLARACIÓN
    (5,   4,   'fiscalyear_receiver',                 Number),  # EJERCICIO
    (9,   9,   'presenter_nif',                       Char),  # NIF DEL DECLARANTE
    (18,  9,   'receiver_nif',                        Char),  # NIF DEL PERCEPTOR
    (27,  9,   'representative_nif',                  Char),  # NIF DEL REPRESENTANTE LEGAL
    (36,  40,  'receiver_name',                       Char),  # APELLIDOS Y NOMBRE, RAZÓN SOCIAL O DENOMINACIÓN DEL PERCEPTOR
    (76,  2,   'province_code',                       Number),  # CÓDIGO PROVINCIA
    (78,  1,   'modality',                            Number),  # MODALIDAD
    (79,  14,  'receiver_withholding_base',           Numeric(sign=SIGN_N_BLANK)),  # BASE RETENCIONES E INGRESOS A CUENTA
    (93,  4,   'receiver_withholding_tax_percentage', Numeric(sign=WITHOUT_SIGN)),  # % RETENCIÓN
    (97,  13,  'receiver_withholding_tax',            Numeric),  # RETENCIONES E INGRESOS A CUENTA
    (110, 4,   'other_fiscalyear',                    Integer),  # EJERCICIO DEVENGO
    (114, 1,   'property_situation',                  Char),  # SITUACIÓN DEL INMUEBLE
    (115, 20,  'property_cadastre_reference',         Char),  # REFERENCIA CATASTRAL
    # DIRECCIÓN DEL INMUEBLE
    (135, 5,   'property_type_street',                Char),  # TIPO DE VÍA
    (140, 50,  'street_name',                         Char),  # NOMBRE VÍA PÚBLICA
    (190, 3,   'street_numeration_type',              Char),  # TIPO DE NUMERACIÓN
    (193, 5,   'street_number',                       Number),  # NÚMERO DE CASA
    (198, 3,   'street_number_type',                  Char),  # CALIFICADOR DEL NÚMERO
    (201, 3,   'block_number',                        Char),  # BLOQUE
    (204, 3,   'entrance',                            Char),  # PORTAL
    (207, 3,   'stair',                               Char),  # ESCALERA
    (210, 3,   'floor',                               Char),  # PLANTA O PISO
    (213, 3,   'gate',                                Char),  # PUERTA
    (216, 40,  'address_extra',                       Char),  # COMPLEMENTO
    (256, 30,  'town',                                Char),  # LOCALIDAD O POBLACIÓN
    (286, 30,  'location',                            Char),  # MUNICIPIO
    (316, 5,   'location_id',                         Char),  # CÓDIGO DE MUNICIPIO
    (321, 2,   'property_province_code',              Char),  # CÓDIGO PROVINCIA
    (323, 5,   'postal_code',                         Number),  # CÓDIGO POSTAL
    (328, 173, 'reserved_aeat',                       Char)    # BLANCOS
)

RECORDS = (RECORD_1, RECORD_2, )

def write(record_1, records_2):
    """
    Use this method to write as file this record design
    :param record_1: Declarant record
    :param records_2: Receiver records List
    :return: file
    """
    file = record_1.write() + '\r\n'
    for party_record in records_2:
        file += party_record.write() + '\r\n'
    return file