# -*- coding: utf-8 -*-

from retrofix.fields import *
from retrofix.record import Record

"""
Diseño de registro del modelo 303 (e 2021 v 2.05)
-----------------------------------------------------------------------------------------------------
303 - Orden HAC/646/2021 (ejercicio 2021 - desde periodo 07 y siguientes) actualizado (04/08/2021)

Régimen General. Declaración trimestral.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '303'

# DP30300
HEADER_RECORD = (
    (1,   2,   'open_tag',             Const('<T')),     # Constante.
    (3,   3,   'model',                Const(MODEL)),    # Modelo
    (6,   1,   'page',                 Const('0')),      # Discriminante.
    (7,   4,   'fiscalyear',           Number),          # Ejercicio devengo. (AAAA)
    (11,  2,   'period',               Char),            # Período. (PP)
    (13,  5,   'close_tag',            Const('0000>')),  # Tipo y cierre
    (18,  5,   'open_aux',             Const('<AUX>')),  # Constante
    (23,  70,  'aeat_1',               Char),            # Reservado para la Administración. Rellenar con blancos
    (93,  4,   'program_version',      Char),            # Versión del Programa (Nota 1)
    (97,  4,   'aeat_2',               Char),            # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat',      Char),            # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',               Char),            # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',            Const('</AUX>'))  # Constante
)

FOOTER_RECORD = (
    (1,  3, 'open_tag',   Const('</T')),   # Constante.
    (4,  3, 'model',      Const(MODEL)),   # Modelo
    (7,  1, 'page',       Const('0')),     # Constante.
    (8,  4, 'fiscalyear', Number),         # Ejercicio devengo. (AAAA)
    (12, 2, 'period',     Char),           # Período. (PP)
    (14, 5, 'close_tag',  Const('0000>')), # Constante
)

# DP30301
PAGE1_RECORD = (
    (1,    2,	'model_start',                  Const('<T')),    # Inicio del identificador de modelo y página.
    (3,    3,	'model',                        Const(MODEL)),   # Modelo.
    (6,    5,	'page',                         Const('01000')), # Página.
    (11,   1,	'model_end',                    Const('>')),     # Fin de identificador de modelo.
    (12,   1,	'indicator_complementary_page', Char),           # Indicador de página complementaria.
    (13,   1,	'declaration_type',             Char),           # Tipo Declaración
    # Identificación (1)
    (14,   9,	'nif',                          Char), # NIF
    (23,   80,	'presenter_name',               Char), # Apellidos y nombre o Razón social
    # Devengo (2)
    (103,  4,	'fiscalyear',                   Number), # Ejercicio
    (107,  2,	'period',                       Char),   # Período
    # Identificación (1)
    (109,  1,   'foral_tax_admin',              Boolean(BOOLEAN_12)), # Tributación exclusivamente foral. Sujeto pasivo que tributa exclusivamente a una Administración tributaria Foral con IVA a la importación liquidado por la Aduana pendiente de ingreso
    (110,  1,	'monthly_return_subscription',  Boolean(BOOLEAN_12)), # Sujeto pasivo inscrito en el Registro de devolución mensual (Art. 30 RIVA)
    (111,  1,	'simplificated_regime',         Number),              # Sujeto pasivo que tributa exclusivamente en Régimen Simplificado (RS)
    (112,  1,	'join_liquidation',             Boolean(BOOLEAN_12)), # Autoliquidación conjunta
    (113,  1,	'recc',                         Boolean(BOOLEAN_12)), # Sujeto pasivo acogido al régimen especial de criterio de Caja (art. 163 undecies LIVA)
    (114,  1,	'recc_receiver',                Boolean(BOOLEAN_12)), # Sujeto pasivo destinatario de operaciones acogidas al régimen especial del criterio de caja
    (115,  1,	'special_prorate',              Boolean(BOOLEAN_12)), # Opción por la aplicación de la prorrata especial (art. 103.Dos.1º LIVA)
    (116,  1,	'special_prorate_revocation',   Boolean(BOOLEAN_12)), # Revocación de la opción por la aplicación de la prorrata especial
    (117,  1,	'bankruptcy',                   Boolean(BOOLEAN_12)), # Sujeto pasivo declarado en concurso de acreedores en el presente período de liquidación
    (118,  8,	'auto_bankruptcy_date',         Date('%Y%m%d')),      # Fecha en que se dictó el auto de declaración de concurso
    (126,  1,	'auto_bankruptcy_declaration',  Char),                # Auto de declaración de concurso dictado en el período
    (127,  1,   'vat_book_voluntary',           Boolean(BOOLEAN_12)), # Sujeto pasivo acogido voluntariamente al SII
    (128,  1,   'exonerated_m390',              Number),              # Sujeto pasivo exonerado de la Declaración-resumen anual del IVA, modelo 390
    (129,  1,   'has_operations',               Number),              # Sujeto pasivo con volumen anual de operaciones distinto de cero (art. 121 LIVA)
    # Liquidación (3) - Regimen General - IVA Devengado - Régimen general
    (130,  17,	'accrued_vat_base_1',           Numeric(sign=WITHOUT_SIGN)),  # [01] Base imponible
    (147,  5,	'accrued_vat_percent_1',        Numeric(sign=WITHOUT_SIGN)),  # [02] Tipo %
    (152,  17,	'accrued_vat_tax_1',            Numeric(sign=WITHOUT_SIGN)),  # [03] Cuota
    (169,  17,	'accrued_vat_base_2',           Numeric(sign=WITHOUT_SIGN)),  # [04] Base imponible
    (186,  5,	'accrued_vat_percent_2',        Numeric(sign=WITHOUT_SIGN)),  # [05] Tipo %
    (191,  17,	'accrued_vat_tax_2',            Numeric(sign=WITHOUT_SIGN)),  # [06] Cuota
    (208,  17,	'accrued_vat_base_3',           Numeric(sign=WITHOUT_SIGN)),  # [07] Base imponible
    (225,  5,	'accrued_vat_percent_3',        Numeric(sign=WITHOUT_SIGN)),  # [08] Tipo %
    (230,  17,	'accrued_vat_tax_3',            Numeric(sign=WITHOUT_SIGN)),  # [09] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Adquisiciones intracomunitarias de bienes y servicios
    (247,  17,	'intracommunity_adquisitions_base', Numeric(sign=WITHOUT_SIGN)),  # [10] Base imponible
    (264,  17,	'intracommunity_adquisitions_tax',  Numeric(sign=WITHOUT_SIGN)),  # [11] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Otras operaciones con inversión del sujeto pasivo (excepto. adq. intracom)
    (281,  17,	'other_passive_subject_base',   Numeric(sign=WITHOUT_SIGN)),  # [12] Base imponible
    (298,  17,	'other_passive_subject_tax',    Numeric(sign=WITHOUT_SIGN)),  # [13] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Modificación bases y cuotas
    (315,  17,	'accrued_vat_base_modification', Numeric(sign=SIGN_N)),  # [14] Base imponible
    (332,  17,	'accrued_vat_tax_modification',  Numeric(sign=SIGN_N)),  # [15] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Recargo equivalencia
    (349,  17,	'accrued_re_base_1',            Numeric(sign=WITHOUT_SIGN)),  # [16] Base imponible
    (366,  5,	'accrued_re_percent_1',         Numeric(sign=WITHOUT_SIGN)),  # [17] Tipo %
    (371,  17,	'accrued_re_tax_1',             Numeric(sign=WITHOUT_SIGN)),  # [18] Cuota
    (388,  17,	'accrued_re_base_2',            Numeric(sign=WITHOUT_SIGN)),  # [19] Base imponible
    (405,  5,	'accrued_re_percent_2',         Numeric(sign=WITHOUT_SIGN)),  # [20] Tipo %
    (410,  17,	'accrued_re_tax_2',             Numeric(sign=WITHOUT_SIGN)),  # [21] Cuota
    (427,  17,	'accrued_re_base_3',            Numeric(sign=WITHOUT_SIGN)),  # [22] Base imponible
    (444,  5,	'accrued_re_percent_3',         Numeric(sign=WITHOUT_SIGN)),  # [23] Tipo %
    (449,  17,	'accrued_re_tax_3',             Numeric(sign=WITHOUT_SIGN)),  # [24] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Modificaciones bases y cuotas del recargo de equivalencia
    (466,  17,	'accrued_re_base_modification', Numeric(sign=SIGN_N)),  # [25] Base imponible
    (483,  17,	'accrued_re_tax_modification',  Numeric(sign=SIGN_N)),  # [26] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado
    (500,  17,	'accrued_total_tax',            Numeric(sign=SIGN_N)),  # [27] Total cuota devengada ( [03] + [06] + [09] + [11] + [13] + [15] + [18] + [21] + [24] + [26])
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en operaciones interiores corrientes
    (517,  17,	'deductible_current_domestic_operations_base',    Numeric(sign=WITHOUT_SIGN)),  # [28] Base
    (534,  17,	'deductible_current_domestic_operations_tax',     Numeric(sign=WITHOUT_SIGN)),  # [29] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en operaciones interiores con bienes de inversión
    (551,  17,	'deductible_investment_domestic_operations_base', Numeric(sign=WITHOUT_SIGN)),  # [30] Base
    (568,  17,	'deductible_investment_domestic_operations_tax',  Numeric(sign=WITHOUT_SIGN)),  # [31] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en las importaciones de bienes corrientes
    (585,  17,	'deductible_current_import_operations_base',      Numeric(sign=WITHOUT_SIGN)),  # [32] Base
    (602,  17,	'deductible_current_import_operations_tax',       Numeric(sign=WITHOUT_SIGN)),  # [33] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en las importaciones de bienes de inversión
    (619,  17,	'deductible_investment_import_operations_base',   Numeric(sign=WITHOUT_SIGN)),  # [34] Base
    (636,  17,	'deductible_investment_import_operations_tax',    Numeric(sign=WITHOUT_SIGN)),  # [35] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - En adquisiciones intracomunitarias de bienes y servicios corrientes
    (653,  17,	'deductible_current_intracommunity_operations_base', Numeric(sign=WITHOUT_SIGN)),  # [36] Base
    (670,  17,	'deductible_current_intracommunity_operations_tax',  Numeric(sign=WITHOUT_SIGN)),  # [37] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - En adquisiciones intracomunitarias de bienes de inversión
    (687,  17,	'deductible_investment_intracommunity_operations_base', Numeric(sign=WITHOUT_SIGN)),  # [38] Base
    (704,  17,	'deductible_investment_intracommunity_operations_tax',  Numeric(sign=WITHOUT_SIGN)),  # [39] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Rectificación de deducciones
    (721,  17,	'deductible_regularization_base',                 Numeric(sign=SIGN_N)),  # [40] Base
    (738,  17,	'deductible_regularization_tax',                  Numeric(sign=SIGN_N)),  # [41] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible
    (755,  17,	'deductible_compensations',                       Numeric(sign=SIGN_N)),  # [42] Compensaciones Régimen Especial A.G. y P. - Cuota
    (772,  17,	'deductible_investment_regularization',           Numeric(sign=SIGN_N)),  # [43] Regularización inversiones - Cuota
    (789,  17,	'deductible_pro_rata_regularization',             Numeric(sign=SIGN_N)),  # [44] Regularización por aplicación del porcentaje definitivo de prorrata - Cuota
    (806,  17,	'deductible_total',                               Numeric(sign=SIGN_N)),  # [45] Total a deducir ( [29] + [31] + [33] + [35] + [37] + [39] + [41] + [42] + [43] + [44] ) - Cuota
    (823,  17,	'difference',                                     Numeric(sign=SIGN_N)),  # [46] Resultado régimen general ( [27] - [45] ) - Cuota
    (840,  600,	'reserved_aeat',                                  Char),  # Reservado para la AEAT
    (1440, 13,	'electronic_stamp_aeat',                          Char),  # Reservado para la AEAT - Sello electrónico reservado para la AEAT
    (1453, 12,  'record_end_id',                                  Const(f'</T{MODEL}01000>')),# Indicador de fin de registro
)

# DP30302
PAGE2_RECORD = ()

# DP30303
PAGE3_RECORD = (
    (1,	   2,  'model_start', Const('<T')),    # Inicio del identificador de modelo y página.
    (3,	   3,  'model',       Const('303')),   # Modelo.
    (6,	   5,  'page',        Const('03000')), # Página.
    (11,   1,  'model_end',   Const('>')),     # Fin de identificador de modelo.
    # Información adicional 
    (12,   17, 'intracommunity_deliveries',      Numeric(sign=SIGN_N)),       # [59] Entregas intracomunitarias de bienes y servicios
    (29,   17, 'exports',                        Numeric(sign=SIGN_N)),       # [60] Exportaciones y operaciones asimiladas
    (46,   17, 'not_subject_or_reverse_charge',  Numeric(sign=SIGN_N)),       # [61] Operaciones no sujetas o con inversión del sujeto pasivo que originan el derecho a deducción
    (63,   17, 'field_120',                      Numeric(sign=SIGN_N)),       # [120] Operaciones no sujetas por reglas de localización (excepto las incluidas en la casilla 123)
    (80,   17, 'reserved_aeat_2',                Numeric(sign=SIGN_N)),       # Reservado para la AEAT
    (97,   17, 'field_122',                      Numeric(sign=SIGN_N)),       # [122] Operaciones sujetas con inversión del sujeto pasivo
    (114,  17, 'field_123',                      Numeric(sign=SIGN_N)),       # [123] OSS. Operaciones no sujetas por reglas de localización acogidas a la OSS
    (131,  17, 'field_124',                      Numeric(sign=SIGN_N)),       # [124] OSS. Operaciones sujetas y acogidas a la OSS
    (148,  17, 'recc_deliveries_base',           Numeric(sign=SIGN_N)),       # [62] Importes devengados en período de liquidación según art. 75 LIVA. - Base Imponible
    (165,  17, 'recc_deliveries_tax',            Numeric(sign=SIGN_N)),       # [63] Importes devengados en período de liquidación según art. 75 LIVA. - Cuota
    (182,  17, 'recc_adquisitions_base',         Numeric(sign=SIGN_N)),       # [74] Cuotas de IVA soportados en operaciones que tributen por el régimen especial del criterio de caja conforme a la regla general de devengo contenida en el artículo 75 LIVA. - Base Imponible
    (199,  17, 'recc_adquisitions_tax',          Numeric(sign=SIGN_N)),       # [75] Cuotas totales de IVA soportados en operaciones que tributen por el régimen especial del criterio de caja conforme a la regla general de devengo contenida en el artículo 75 de LIVA. - Cuota
    # Resultado 
    (216,  17, 'tax_regularization',                       Numeric(sign=SIGN_N)),        # [76] Regularización cuotas art. 80.cinco.5ª LIVA
    (233,  17, 'total_difference',                         Numeric(sign=SIGN_N)),        # [64] Suma de resultados ( [46] + [58] + [76] )
    (250,  5,  'state_administration_percent',             Numeric(sign=WITHOUT_SIGN)),  # [65] % Atribuible a la Administración del Estado
    (255,  17, 'state_administration_amount',              Numeric(sign=SIGN_N)),        # [66] Atribuible a la Administración del Estado
    (272,  17, 'import_taxes',                             Numeric(sign=WITHOUT_SIGN)),  # [77] IVA a la importación liquidado por la Aduana pendiente de ingreso
    (289,  17, 'amount_pdte_to_compensate',                Numeric(sign=WITHOUT_SIGN)),  # [110] Cuotas a compensar pendientes de periodos anteriores
    (306,  17, 'amount_to_compensate',                     Numeric(sign=WITHOUT_SIGN)),  # [78] Cuotas a compensar de periodos anteriores aplicadas en este periodo
    (323,  17, 'previous_period_amount_to_compensate',     Numeric(sign=WITHOUT_SIGN)),  # [87] Cuotas a compensar de periodos previos pendientes para periodos posteriores ([110] - [78])
    (340,  17, 'joint_taxation_state_provincial_councils', Numeric(sign=SIGN_N)),        # [68] Exclusivamente para sujetos pasivos que tributan conjuntamente a la Administración del Estado y a las Diputaciones Forales Resultado de la regularización anual
    (357,  17, 'result',                                   Numeric(sign=SIGN_N)),        # [69]Resultado ( [66] + [77] - [78] + [68] )
    (374,  17, 'to_deduce',                                Numeric(sign=SIGN_N)),        # [70] A deducir
    (391,  17, 'liquidation_result',                       Numeric(sign=SIGN_N)),        # [71] Resultado de la liquidación ( [69] - [70] )
    (408,  1,  'complementary_autoliquidation',            Boolean(BOOLEAN_X)),  # Declaración complementaria
    (409,  13, 'previous_declaration_receipt',             Char),  # Número justificante declaración anterior
    (422,  1,  'without_activity',                         Boolean(BOOLEAN_X)),  # Declaración Sin actividad
    (423,  11, 'swiftbic',                                 Char),  # Devolución. SWIFT-BIC
    (434,  34, 'bank_account',                             Char),  # Domiciliación/Devolución - IBAN
    (468,  17, 'reserved_aeat_6',                          Char),  # Reservado para la AEAT
    (485,  70, 'return_bank_name',                         Char),  # Devolución - Banco/Bank name
    (555,  35, 'return_bank_address',                      Char),  # Devolución - Dirección del Banco/ Bank address
    (590,  30, 'return_city',                              Char),  # Devolución – Ciudad/City
    (620,  2,  'return_country_code',                      Char),  # Devolución - Código País/Country code
    (622,  1,  'return_SEPA',                              Char),  # Devolución - Marca SEPA
    (623,  445,'reserved_aeat_7',                          Char),  # Reservado para la AEAT
    (1068, 12, 'record_end_id',                            Const('</T30303000>')), # Indicador de fin de registro
)

# DP30304
PAGE4_RECORD = ()

RECORDS = (PAGE1_RECORD, PAGE2_RECORD, PAGE3_RECORD, PAGE4_RECORD, )


def write(header_record, footer_record, record_page1=None, record_page2=None, record_page3=None, record_page4=None):
    """
    Use this method to write as file this record design
    :param header_record: Record using HEADER_RECORD structure
    :param footer_record: Record using FOOTER_RECORD structure
    :param record_page1: Record using PAGE1_RECORD structure
    :param record_page2: Record using PAGE2_RECORD structure
    :param record_page3: Record using PAGE3_RECORD structure
    :param record_page4: Record using PAGE4_RECORD structure
    :return: file
    """
    file = header_record.write()
    if record_page1:
        file += record_page1.write()
    if record_page2:
        file += record_page2.write()
    if record_page3:
        file += record_page3.write()
    if record_page4:
        file += record_page4.write()
    file += footer_record.write()

    return file


def read(data):
    lines = data.splitlines()
    records = []
    current_line = lines.pop(0)
    records.append(Record.extract(current_line, PAGE1_RECORD))
    return records
