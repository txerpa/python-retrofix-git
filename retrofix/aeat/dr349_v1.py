# -*- coding: utf-8 -*-

from retrofix.exception import RetrofixException
from retrofix.fields import Const, Char, Number, Integer, Numeric, BOOLEAN_X, Boolean
from retrofix.record import Record

##############################################################################
#
#    Copyright (C) 2013-2016 NaN Projectes de Programari Lliure, S.L.
#                           http://www.NaN-tic.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


"""
Diseño de registro del modelo 349 (v 1)
-----------------------------------------------------------------------------------------------------
349 - Orden EHA/769/2010 (modificada por Orden EHA/1721/2011)
  
Declaración recapitulativa de operaciones intracomunitarias.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '349'

# Tipo 1: Registro del declarante:
RECORD_1 = (
    (  1,  1, 'record_type',                 Const('1')),            # TIPO DE REGISTRO
    (  2,  3, 'model',                       Const(MODEL)),          # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',                  Number),                # EJERCICIO
    (  9,  9, 'nif',                         Char),                  # NIF DEL DECLARANTE
    ( 18, 40, 'presenter_name',              Char),                  # APELLIDOS Y NOMBRE O RAZÓN SOCIAL DEL DECLARANTE
    ( 58,  1, 'support_type',                Char),                  # TIPO DE SOPORTE
    # PERSONA CON QUIÉN RELACIONARSE
    ( 59,  9, 'contact_phone',               Number),                # TELÉFONO
    ( 68, 40, 'contact_name',                Char),                  # APELLIDOS Y NOMBRE

    (108, 13, 'declaration_number',          Integer),               # NUMERO IDENTIFICATIVO  DE LA DECLARACION
    # DECLARACION COMPLEMENTARIA O SUSTITUTIVA
    (121,  1, 'complementary',               Char),                  # DECLARACIÓN COMPLEMENTARIA
    (122,  1, 'replacement',                 Char),                  # DECLARACIÓN SUSTITUTIVA

    (123, 13, 'previous_declaration_number', Number),                # NUMERO IDENTIFICATIVO  DE LA DECLARACIÓN ANTERIOR
    (136,  2, 'period',                      Char),                  # PERÍODO
    (138,  9, 'operation_count',             Integer),               # NÚMERO TOTAL DE OPERADORES INTRACOMUNITARIOS
    (147, 15, 'operation_amount',            Numeric),               # IMPORTE DE LAS OPERACIONES INTRACOMUNITARIAS
    (162,  9, 'ammendment_count',            Integer),               # NÚMERO TOTAL DE OPERADORES INTRACOMUNITARIOS CON RECTIFICACIONES
    (171, 15, 'ammendment_amount',           Numeric),               # IMPORTE DE LAS RECTIFICACIONES
    (186,  1, 'monthly_periodicity',         Boolean(BOOLEAN_X)),    # INDICADOR CAMBIO PERIODICIDAD EN LA OBLIGACIÓN DE DECLARAR
    (391,  9, 'representative_nif',          Char),                  # NIF DEL REPRESENTANTE LEGAL
    (488, 13, 'digital_signature', Char),                            # SELLO ELECTRONICO
    )

# Tipo 2: Registro de operador intracomunitario
RECORD_2_OPERA = (
    (  1,  1, 'record_type',   Const('2')),   # TIPO DE REGISTRO
    (  2,  3, 'model',         Const(MODEL)), # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',    Number),       # EJERCICIO
    (  9,  9, 'nif',           Char),         # NIF DEL DECLARANTE
    ( 76, 17, 'party_vat',     Char),         # NIF OPERADOR COMUNITARIO
    ( 93, 40, 'party_name',    Char),         # APELLIDOS Y NOMBRE O RAZÓN SOCIAL DEL OPERADOR INTRACOMUNITARIO
    (133,  1, 'operation_key', Char),         # CLAVE DE OPERACIÓN
    (134, 13, 'base',          Numeric),      # BASE IMPONIBLE
    (147, 354, 'blank',        Char),
    )

# Tipo 2: Registro de rectificaciones
RECORD_2_AMEND = (
    (  1,  1, 'record_type',           Const('2')),   # TIPO DE REGISTRO
    (  2,  3, 'model',                 Const(MODEL)), # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',            Number),       # EJERCICIO
    (  9,  9, 'nif',                   Char),         # NIF DEL DECLARANTE
    ( 76, 17, 'party_vat',             Char),         # NIF OPERADOR COMUNITARIO
    ( 93, 40, 'party_name',            Char),         # APELLIDOS Y NOMBRE O RAZÓN SOCIAL DEL OPERADOR INTRACOMUNITARIO
    (133,  1, 'operation_key',         Char),         # CLAVE DE OPERACIÓN
    # RECTIFICACIONES
    (147,  4, 'ammendment_fiscalyear', Number),       # Ejercicio
    (151,  2, 'ammendment_period',     Char),         # Periodo
    (153, 13, 'base',                  Numeric),      # Base Imponible Rectificada
    (166, 13, 'original_base',         Numeric),      # Base imponible declarada anteriormente
    (179, 322, 'blank',                Char),
    )

RECORDS = (RECORD_1, RECORD_2_OPERA, RECORD_2_AMEND, )


def write(declarant_record, operator_records=[], amendment_records=[]):
    """
    Use this method to write as file this record design
    :param declarant_record: Declarant record
    :param operator_records: Operator records List
    :param amendment_records: Amendment records List
    :return: file
    """
    file = declarant_record.write() + '\r\n'

    for record_2 in operator_records:
        file += record_2.write() + '\r\n'

    for record_2 in amendment_records:
        file += record_2.write() + '\r\n'

    return file


def read(data):
    lines = data.splitlines()
    records = []

    current_line = lines.pop(0)
    records.append(Record.extract(current_line, RECORD_1))

    current_line = lines.pop(0)
    while lines:
        if Record.valid(current_line, RECORD_2_OPERA):
            record = Record.extract(current_line, RECORD_2_OPERA)
        if Record.valid(current_line, RECORD_2_AMEND):
            record = Record.extract(current_line, RECORD_2_AMEND)
        else:
            raise RetrofixException(f'Invalid record: {current_line}')
        records.append(record)
        current_line = lines.pop(0)
    return records
