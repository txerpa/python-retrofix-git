# -*- coding: utf-8 -*-

from retrofix.fields import Const, Number, Char, Numeric, Integer, SIGN_N, BOOLEAN_X, Boolean, WITHOUT_SIGN

"""
Diseño de registro del modelo 111 (v 1.7)
-----------------------------------------------------------------------------------------------------
111 - Orden EHA/3127/2009 (Ejercicios 2016 y siguientes)

Retenciones e ingresos a cuenta. 
Rendimientos del trabajo, de actividades profesionales, de actividades agrícolas y ganaderas y premios.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '111'

# M11100
HEADER_RECORD = (
    (1,   2,   'open_tag',                                   Const('<T')),         # Constante.
    (3,   3,   'model',                                      Const(MODEL)),        # Modelo
    (6,   1,   'page',                                       Const('0')),          # Constante.
    (7,   4,   'fiscalyear',                                 Number),              # Ejercicio devengo. (AAAA)
    (11,  2,   'period',                                     Char),                # Período. (PP)
    (13,  5,   'close_tag',                                  Const('0000>')),      # Constante
    (18,  5,   'open_aux',                                   Const('<AUX>')),      # Constante
    (23,  70,  'aeat_1',                                     Char),                # Reservado para la Administración. Rellenar con blancos
    (93,  4,   'program_version',                            Char),                # Versión del Programa (Nota 1)
    (97,  4,   'aeat_2',                                     Char),                # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat',                            Char),                # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',                                     Char),                # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',                                  Const('</AUX>'))      # Constante
)                                                                                  
                                                                                   
FOOTER_RECORD = (                                                                  
    (1, 3,     'open_tag',                                   Const('</T')),        # Constante.
    (4, 3,     'model',                                      Const(MODEL)),        # Modelo
    (7, 1,     'page',                                       Const('0')),          # Constante.
    (8, 4,     'fiscalyear',                                 Number),              # Ejercicio devengo. (AAAA)
    (12, 2,    'period',                                     Char),                # Período. (PP)
    (14, 5,    'close_tag',                                  Const('0000>')),      # Constante
)

# M11101
RECORD = (
    (1,	  2,   'record_start',                               Const('<T')),   # Inicio del identificador de modelo y página
    (3,	  3,   'model',                                      Const(MODEL)),  # Modelo
    (6,	  2,   'page',                                       Const('01')),   # Página
    (8,	  4,   'model_end',                                  Const('000>')), # Fin de identificador de modelo
    (12,  1,   'complementary_field',                        Const(' ')),    # Indicador de página complementaria
    (13,  1,   'declaration_type',                           Char),          # Tipo de declaración
    # Identificación.
    (14,  9,   'nif',                                        Char),  # Sujeto pasivo. NIF
    (23,  60,  'subject_surname',                            Char),  # Sujeto pasivo. Denominación o Apellidos
    (83,  20,  'subject_name',                               Char),  # Sujeto pasivo. Nombre
    (103, 4,   'fiscalyear',                                 Number),  # Ejercicio
    (107, 2,   'period',                                     Char),  # Periodo
    # Rendim. del trabajo - Rendimientos dinerarios
    (109, 8,   'employment_income_number_receivers',         Integer),  # Nº de perceptores
    (117, 17,  'employment_income_base',                     Numeric(sign=SIGN_N)),  # Importe percepciones
    (134, 17,  'employment_income_tax',                      Numeric(sign=SIGN_N)),  # Importe retenciones
    # Rendim. del trabajo - Rendimientos en especie
    (151, 8,   'employment_in_kind_income_number_receivers', Integer),  # Nº de perceptores
    (159, 17,  'employment_in_kind_income_base',             Numeric(sign=SIGN_N)),  # Valor percepciones en especie
    (176, 17,  'employment_in_kind_income_tax',              Numeric(sign=SIGN_N)),  # Importe ingresos a cuenta
    # Rendim. actividades económicas - Rendimientos dinerarios 
    (193, 8,   'activity_income_number_receivers',           Integer),  # Nº de perceptores
    (201, 17,  'activity_income_base',                       Numeric(sign=SIGN_N)),  # Importe percepciones
    (218, 17,  'activity_income_tax',                        Numeric(sign=WITHOUT_SIGN)),  # Importe retenciones
    # Rendim. actividades económicas - Rendimientos en especie 
    (235, 8,   'activity_in_kind_income_number_receivers',   Integer),  # Nº de perceptores
    (243, 17,  'activity_in_kind_income_base',               Numeric(sign=SIGN_N)),  # Valor percepciones en especie
    (260, 17,  'activity_in_kind_income_tax',                Numeric(sign=SIGN_N)),  # Importe de los ingresos a cuenta
    # Premios- Premios dinerarios
    (277, 8,   'prize_number_receivers',                     Integer),  # Nº  de perceptores
    (285, 17,  'prize_base',                                 Numeric(sign=SIGN_N)),  # Importe de las percepciones
    (302, 17,  'prize_tax',                                  Numeric(sign=SIGN_N)),  # Importe de las retenciones
    # Premios - Premios en especie
    (319, 8,   'prize_in_kind_number_receivers',             Integer),  # Nº de perceptores
    (327, 17,  'prize_in_kind_base',                         Numeric(sign=SIGN_N)),  # Valor percepciones en especie
    (344, 17,  'prize_in_kind_tax',                          Numeric(sign=SIGN_N)),  # Importe de los ingresos a cuenta
    # Ganancias patrim. Aprovecham. Forestales - Percep. dinerarias 
    (361, 8,   'capital_gain_number_receivers',              Integer),  # Nº perceptores
    (369, 17,  'capital_gain_base',                          Numeric(sign=SIGN_N)),  # Importe percepciones
    (386, 17,  'capital_gain_tax',                           Numeric(sign=SIGN_N)),  # Importe retenciones
    # Ganancias patrim. Aprovecham. Forestales - Percep. en especie 
    (403, 8,   'capital_gain_in_kind_number_receivers',      Integer),  # Nº perceptores
    (411, 17,  'capital_gain_in_kind_base',                  Numeric(sign=SIGN_N)),  # Importe percepciones
    (428, 17,  'capital_gain_in_kind_tax',                   Numeric(sign=SIGN_N)),  # Importe ingresos a cuenta
    # Contraprest. cesión dchos. imagen                      
    (445, 8,   'image_rights_release_number_receivers',      Integer),  # Nº de perceptores
    (453, 17,  'image_rights_release_base',                  Numeric(sign=SIGN_N)),  # Contraprestaciones satisfechas
    (470, 17,  'image_rights_release_tax',                   Numeric(sign=SIGN_N)),  # Importe de los ingresos a cuenta
    # Total liquidación                                      
    (487, 17,  'total_tax_sum',                              Numeric(sign=SIGN_N)),  # Suma retenciones e ingresos a cuenta
    (504, 17,  'previous_declaration_result',                Numeric(sign=SIGN_N)),  # Resultado de anteriores declaraciones
    (521, 17,  'total_of_declaration',                       Numeric(sign=SIGN_N)),  # Resultado a ingresar

    (538, 1,   'complementary_autoliquidation',              Boolean(BOOLEAN_X)),  # Declaración complementaria
    (539, 13,  'previous_declaration_receipt',               Char),  # Número de justificante de la declaración anterior
    (552, 1,   'reserved_school',                            Char),  # Reservado. Administración presentando declaración de Colegio Concertado (CC)
    (553, 34,  'iban',                                       Char),  # Domiciliación - IBAN
    (587, 389, 'reserved_aeat',                              Char),  # Reservado para la Administración
    (976, 13,   'electronic_stamp_aeat',                     Char),  # Reservado para el sello electrónico de la AEAT
    (989, 12,   'record_end',                                Const(f'</T{MODEL}01000>')) # Indicador de fin de registro
)

RECORDS = (RECORD, )

def write(header_record, record, footer_record):
    """
    Use this method to write as file this record design
    :param header_record: Record using HEADER_RECORD structure
    :param record: Record using RECORD structure
    :param footer_record: Record using FOOTER_RECORD structure
    :return: file
    """
    return header_record.write() + record.write() + footer_record.write()

