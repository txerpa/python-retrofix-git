# -*- coding: utf-8 -*-

from retrofix.exception import RetrofixException
from retrofix.fields import Const, Number, Char, Integer, Numeric, Boolean
from retrofix.fields import SIGN_N_BLANK, BOOLEAN_X
from retrofix.record import Record

##############################################################################
#
#    Copyright (C) 2013-2016 NaN Projectes de Programari Lliure, S.L.
#                           http://www.NaN-tic.com
#    Copyright (C) 2015 Zikzakmedia S.L.
#                           http://www.zikzakmedia.com
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


"""
Diseño de registro del modelo 347 (v 1)
-----------------------------------------------------------------------------------------------------
347 - Orden HAP/1732/2014, de 24 de septiembre 
  
Declaración anual operaciones con terceras personas.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '347'

# Tipo 1: Registro del declarante:
RECORD_1 = (
    (  1,  1, 'record_type',                    Const('1')),                 # TIPO DE REGISTRO
    (  2,  3, 'model',                          Const(MODEL)),               # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',                     Number),                     # EJERCICIO
    (  9,  9, 'nif',                            Char),                       # NIF DEL DECLARANTE
    ( 18, 40, 'presenter_name',                 Char),                       # APELLIDOS Y NOMBRE O RAZÓN SOCIAL DEL DECLARANTE
    ( 58,  1, 'support_type',                   Char),                       # TIPO DE SOPORTE.
    # PERSONA CON QUIÉN RELACIONARSE
    ( 59,  9, 'contact_phone',                  Number),                     # TELÉFONO
    ( 68, 40, 'contact_name',                   Char),                       # APELLIDOS Y NOMBRE

    (108, 13, 'declaration_number',             Integer),                    # NÚMERO IDENTIFICATIVO DE LA DECLARACIÓN
    # DECLARACION COMPLEMENTARIA O SUSTITUTIVA
    (121,  1, 'complementary',                  Char),                       # DECLARACIÓN COMPLEMENTARIA
    (122,  1, 'replacement',                    Char),                       # DECLARACIÓN SUSTITUTIVA

    (123, 13, 'previous_declaration_number',    Number),                     # NÚMERO IDENTIFICATIVO DE LA DECLARACIÓN ANTERIOR
    (136,  9, 'party_count',                    Integer),                    # NÚMERO TOTAL DE PERSONAS Y ENTIDADES
    (145, 16, 'party_amount',                   Numeric(sign=SIGN_N_BLANK)), # IMPORTE TOTAL ANUAL DE LAS OPERACIONES
    (161,  9, 'property_count',                 Integer),                    # NÚMERO TOTAL DE INMUEBLES
    (170, 16, 'property_amount',                Numeric(sign=SIGN_N_BLANK)), # IMPORTE TOTAL DE LAS OPERACIONES DE ARRENDAMIENTO DE LOCALES DE NEGOCIO
    (391,  9, 'representative_nif',             Char),                       # NIF DEL REPRESENTANTE LEGAL
    (488, 13, 'digital_signature',              Char),                       # SELLO ELECTRONICO
    )

# Tipo 2: Registro de declarado
RECORD_2 = (
    (  1,  1, 'record_type',                    Const('2')),                 # TIPO DE REGISTRO
    (  2,  3, 'model',                          Const(MODEL)),               # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',                     Number),                     # EJERCICIO
    (  9,  9, 'nif',                            Char),                       # NIF DEL DECLARANTE
    ( 18,  9, 'party_nif',                      Char),                       # NIF DEL DECLARADO
    ( 27,  9, 'representative_nif',             Char),                       # NIF DEL REPRESENTANTE LEGAL
    ( 36, 40, 'party_name',                     Char),                       # APELLIDOS Y NOMBRE, RAZÓN SOCIAL O DENOMINACIÓN DEL DECLARADO
    ( 76,  1, 'sheet_type',                     Const('D')),                 # TIPO DE HOJA
    ( 77,  2, 'province_code',                  Number),                     # CÓDIGO PROVINCIA/PAIS
    ( 79,  2, 'country_code',                   Char),                       # CÓDIGO PAÍS
    ( 82,  1, 'operation_key',                  Char),                       # CLAVE OPERACIÓN
    ( 83, 16, 'amount',                         Numeric(sign=SIGN_N_BLANK)), # IMPORTE ANUAL DE LAS OPERACIONES
    ( 99,  1, 'insurance',                      Boolean(BOOLEAN_X)),         # OPERACIÓN SEGURO
    (100,  1, 'business_premises_rent',         Boolean(BOOLEAN_X)),         # ARRENDAMIENTO LOCAL NEGOCIO
    (101, 15, 'cash_amount',                    Numeric),                    # IMPORTE PERCIBIDO EN METÁLICO
    (116, 16, 'vat_liable_property_amount',     Numeric(sign=SIGN_N_BLANK)), # IMPORTE ANUAL PERCIBIDO POR TRANSMISIONES DE INMUEBLES SUJETAS A IVA
    (132,  4, 'fiscalyear_cash_operation',      Number),                     # EJERCICIO
    # PRIMER TRIMESTRE
    (136, 16, 'first_quarter_amount',           Numeric(sign=SIGN_N_BLANK)), # IMPORTE DE LAS OPERACIONES
    (152, 16, 'first_quarter_property_amount',  Numeric(sign=SIGN_N_BLANK)), # IMPORTE PERCIBIDO POR TRANSMISIONES DE INMUEBLES SUJETAS A IVA
    # SEGUNDO TRIMESTRE
    (168, 16, 'second_quarter_amount',          Numeric(sign=SIGN_N_BLANK)), # IMPORTE DE LAS OPERACIONES 
    (184, 16, 'second_quarter_property_amount', Numeric(sign=SIGN_N_BLANK)), # IMPORTE PERCIBIDO POR TRANSMISIONES DE INMUEBLES SUJETAS A IVA
    # TERCER TRIMESETRE
    (200, 16, 'third_quarter_amount',           Numeric(sign=SIGN_N_BLANK)), # IMPORTE DE LAS OPERACIONES
    (216, 16, 'third_quarter_property_amount',  Numeric(sign=SIGN_N_BLANK)), # IMPORTE PERCIBIDO POR TRANSMISIONES DE INMUEBLES SUJETAS A IVA
    # CUARTO TRIMESTRE
    (232, 16, 'fourth_quarter_amount',          Numeric(sign=SIGN_N_BLANK)), # IMPORTE DE LAS OPERACIONES
    (248, 16, 'fourth_quarter_property_amount', Numeric(sign=SIGN_N_BLANK)), # IMPORTE PERCIBIDO POR TRANSMISIONES DE INMUEBLES SUJETAS A IVA
    
    (264, 17, 'community_vat',                  Char),                       # NIF OPERADOR COMUNITARIO
    (281,  1, 'cash_vat_operation',             Boolean(BOOLEAN_X)),         # OPERACIONES RÉGIMEN ESPECIAL CRITERIO DE CAJA IVA
    (282,  1, 'tax_person_operation',           Boolean(BOOLEAN_X)),         # OPERACIÓN CON INVERSIÓN DEL SUJETO PASIVO
    (283,  1, 'related_goods_operation',        Boolean(BOOLEAN_X)),         # OPERACION CON BIENES VINCULADOS O DESTINADOS A VINCULARSE AL RÉGIMEN DE DEPÓSITO DISTINTO DEL ADUANERO
    (284, 16, 'cash_vat_criteria',              Numeric(sign=SIGN_N_BLANK)), # IMPORTE ANUAL DE LAS OPERACIONES DEVENGADAS CONFORME AL CRITERIO DE CAJA DEL IVA
    (300, 201, 'blank', Char),
    )

# Tipo 2 -B: Registro de inmueble
RECORD_2_PROPERTY = (
    (  1,  1, 'record_type',                    Const('2')),                 # TIPO DE REGISTRO
    (  2,  3, 'model',                          Const(MODEL)),               # MODELO DECLARACIÓN
    (  5,  4, 'fiscalyear',                     Number),                     # EJERCICIO
    (  9,  9, 'nif',                            Char),                       # NIF DEL DECLARANTE
    ( 18,  9, 'party_nif',                      Char),                       # NIF DEL ARRENDATARIO
    ( 27,  9, 'representative_nif',             Char),                       # NIF DEL REPRESENTANTE LEGAL
    ( 36, 40, 'party_name',                     Char),                       # APELLIDOS Y NOMBRE, RAZÓN SOCIAL O DENOMINACIÓN DEL ARRENDATARIO
    ( 76,  1, 'sheet_type',                     Const('I')),                 # TIPO DE HOJA
    ( 99, 16, 'amount',                         Numeric(sign=SIGN_N_BLANK)), # IMPORTE DE LA OPERACION
    (115,  1, 'situation',                      Number),                     # SITUACIÓN DEL INMUEBLE
    (116, 25, 'cadaster_number',                Char),                       # REFERENCIA CATASTRAL
    # DIRECCIÓN DEL INMUEBLE
    (141,  5, 'road_type',                      Char),                       # TIPO DE VÍA
    (146, 50, 'street',                         Char),                       # NOMBRE VÍA PÚBLICA
    (196,  3, 'number_type',                    Char),                       # TIPO DE NUMERACIÓN
    (199,  5, 'number',                         Number),                     # NÚMERO DE CASA
    (204,  3, 'number_qualifier',               Char),                       # CALIFICADOR DEL NÚMERO
    (207,  3, 'block',                          Char),                       # BLOQUE
    (210,  3, 'doorway',                        Char),                       # PORTAL
    (213,  3, 'stair',                          Char),                       # ESCALERA
    (216,  3, 'floor',                          Char),                       # PLANTA O PISO
    (219,  3, 'door',                           Char),                       # PUERTA
    (222, 40, 'complement',                     Char),                       # COMPLEMENTO
    (262, 30, 'city',                           Char),                       # LOCALIDAD O POBLACIÓN
    (292, 30, 'municipality',                   Char),                       # MUNICIPIO
    (322,  5, 'municipality_code',              Char),                       # CÓDIGO DE MUNICIPIO
    (327,  2, 'province_code',                  Number),                     # CÓDIGO PROVINCIA
    (329,  5, 'zip',                            Number),                     # CÓDIGO POSTAL
    (334, 167, 'blank',                         Char),
    )

RECORDS = (RECORD_1, RECORD_2, RECORD_2_PROPERTY, )


def write(declarant_record, party_records=[], property_records=[]):
    """
    Use this method to write as file this record design
    :param declarant_record: Declarant record
    :param party_records: Party records List
    :param property_records: Party records List
    :return: file
    """
    file = declarant_record.write() + '\r\n'

    for record_2 in party_records:
        file += record_2.write() + '\r\n'

    for record_2 in property_records:
        file += record_2.write() + '\r\n'

    return file

def read(data):
    lines = data.splitlines()
    records = []

    current_line = lines.pop(0)
    records.append(Record.extract(current_line, RECORD_1))

    current_line = lines.pop(0)
    while lines:
        if Record.valid(current_line, RECORD_2):
            record = Record.extract(current_line, RECORD_2)
        if Record.valid(current_line, RECORD_2_PROPERTY):
            record = Record.extract(current_line, RECORD_2_PROPERTY)
        else:
            raise RetrofixException(f'Invalid record: {current_line}')
        records.append(record)
        current_line = lines.pop(0)
    return records
