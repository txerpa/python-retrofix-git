# -*- coding: utf-8 -*-

from retrofix.fields import Char, Const, Number, SIGN_N, WITHOUT_SIGN, Numeric, Boolean, BOOLEAN_01, Integer

"""
Diseño de registro del modelo 390 (v 3)
-----------------------------------------------------------------------------------------------------
390 - Ejercicio 2017  

Declaración resumen anual IVA.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '390'

# Pag. 0 Header
HEADER_RECORD = (
    (1, 2,     'open_tag',        Const('<T')),    # Inicio del identificador de modelo y página.
    (3, 3,     'model',           Const(MODEL)),   # Modelo
    (6, 1,     'page',            Const('0')),     # Discriminante.
    (7, 4,     'fiscalyear',      Number),         # Ejercicio de devengo (EEEE)
    (11, 2,    'period',          Const('0A')),    # Periodo (PP)
    (13, 5,    'close_tag',       Const('0000>')), # Tipo y cierre
    (18, 5,    'open_aux',        Const('<AUX>')), # Constante
    (23, 70,   'aeat_1',          Char),           # Reservado para la Administración. Rellenar con blancos
    (93, 4,    'program_version', Char),           # Versión del Programa (Nota 1)
    (97, 4,    'aeat_2',          Char),           # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat', Char),           # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',          Char),           # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',       Const('</AUX>')) # Constante
)
# Pag. 0 Footer
FOOTER_RECORD = (
    (1, 3, 'footer_start', Const('</T')),   # Constante.
    (4, 3, 'model',        Const(MODEL)),   # Modelo
    (7, 1, 'page',         Const('0')),     # Constante.
    (8, 4, 'fiscalyear',   Number),            # Ejercicio devengo. (AAAA)
    (12, 2, 'period',      Const('0A')),    # Período. (PP)
    (14, 5, 'footer_end',  Const('0000>')), # Constante
    (19, 2, 'record_end',  Const('\r\n'))   # Fin de Registro. Constante CRLF( Hexadecimal 0D0A, Decimal 1310)
)

# Pag. 1
PAGE1_RECORD = (
    (1,   2,  'model_start',        Const('<T')),    # Inicio del identificador de modelo y página.
    (3,   3,  'model',              Const(MODEL)),   # Modelo.
    (6,   5,  'page',               Const('01000')), # Página.
    (11,  1,  'model_end',          Const('>')),     # Fin de identificador de modelo.
    (12,  1,  'complementary_page', Const(' ')),     # Indicador de página complementaria
    (13,  1,  'aeat',               Char),           # RESERVADO PARA LA A.E.A.T. (Dejar en blanco)
    (14,  9,  'nif',                Char),           # Identificación. Sujeto pasivo. NIF
    (23,  60, 'subject_surname',    Char),           # Identificación. Sujeto pasivo. Denominación o Apellidos
    (83,  20, 'subject_name',       Char),           # Identificación. Sujeto pasivo. Nombre
    (103, 4,  'fiscalyear',         Number),         # Identificación. Ejercicio
    (107, 2,  'aeat2',              Char),           # RESERVADO PARA LA A.E.A.T. (Dejar en blanco)
    # 1. Sujeto pasivo
    (109, 1, 'n1_sujeto_pasivo_registro_de_devolucion_mensual',                                    Boolean(BOOLEAN_01)), # Registro de devolución mensual.
    (110, 1, 'n1_sujeto_pasivo_regimen_especial_del_grupo_de_entidades',                           Boolean(BOOLEAN_01)), # Regimen especial del grupo de entidades
    (111, 7, 'n1_sujeto_pasivo_numero_de_grupo',                                                   Char), # Número de grupo
    (118, 1, 'n1_sujeto_pasivo_dominante',                                                         Boolean(BOOLEAN_01)), # dominante?
    (119, 1, 'n1_sujeto_pasivo_dependiente',                                                       Boolean(BOOLEAN_01)), # dependiente?
    (120, 1, 'n1_sujeto_pasivo_tipo_regimen_especial_aplicable_art_163_sexiescinco_si_o_no',       Integer), # Tipo régimen especial aplicable. Art 163 sexies.cinco. Si o No
    (121, 9, 'n1_sujeto_pasivo_nif_entidad_dominante',                                             Char), # NIF entidad dominante
    (130, 1, 'n1_sujeto_pasivo_autoliquidacion_del_ultimo_periodo_pertenece_a_grupo_de_entidades', Integer), # Autoliquidación del último período pertenece a grupo de entidades.
    (131, 1, 'n1_sujeto_pasivo_concurso_acreedores_en_este_ejercicio',                             Integer), # Concurso acreedores en este ejercicio
    (132, 1, 'n1_sujeto_pasivo_regimen_especial_del_criterio_de_caja',                             Integer), # Regimen especial del criterio de caja
    (133, 1, 'n1_sujeto_pasivo_ha_sido_destinatario_del_regimen_especial_del_criterio_de_caja',    Integer), # Ha sido destinatario del régimen especial del criterio de caja
    # 2. Devengo
    (134, 1,  'n2_devengo_sustitutiva',                                                 Boolean(BOOLEAN_01)), # Sustitutiva?
    (135, 1,  'n2_devengo_sustitutiva_por_rectificacion_de_cuotas',                     Boolean(BOOLEAN_01)), # Sustitutiva por rectificación de cuotas?
    (136, 13, 'n2_devengo_no_justificante_declaracion_anterior',                        Char),                  # Nº justificante declaración anterior
    # 3. Datos estadísticos
    (149, 40, 'n3_datos_estadisticos_a_actividades_principal',                          Char),                  # A - Actividades - Principal
    (189, 1,  'n3_datos_estadisticos_b_clave_principal',                                Number), # B - Clave - Principal
    (190, 4,  'n3_datos_estadisticos_c_epigrafe_iae_principal',                         Char),                  # C -Epígrafe I.A.E. - Principal
    (194, 40, 'n3_datos_estadisticos_a_actividades_otras_1a',                           Char),                  # A - Actividades - Otras - 1ª
    (234, 1,  'n3_datos_estadisticos_b_clave_otras_1a',                                 Number),                # B - Clave - Otras 1ª
    (235, 4,  'n3_datos_estadisticos_c_epigrafe_iae_otras_1o',                          Char),                  # C -Epígrafe I.A.E. - Otras 1º
    (239, 40, 'n3_datos_estadisticos_a_actividades_otras_2a',                           Char),                  # A - Actividades - Otras - 2ª
    (279, 1,  'n3_datos_estadisticos_b_clave_otras_2a',                                 Number),                # B - Clave - Otras 2ª
    (280, 4,  'n3_datos_estadisticos_c_epigrafe_iae_otras_2o',                          Char),                  # C -Epígrafe I.A.E. - Otras 2º
    (284, 40, 'n3_datos_estadisticos_a_actividades_otras_3a',                           Char),                  # A - Actividades - Otras - 3ª
    (324, 1,  'n3_datos_estadisticos_b_clave_otras_3a',                                 Number),                # B - Clave - Otras 3ª
    (325, 4,  'n3_datos_estadisticos_c_epigrafe_iae_otras_3o',                          Char),                  # C -Epígrafe I.A.E. - Otras 3º
    (329, 40, 'n3_datos_estadisticos_a_actividades_otras_4a',                           Char),                  # A - Actividades - Otras - 4ª
    (369, 1,  'n3_datos_estadisticos_b_clave_otras_4a',                                 Number),                # B - Clave - Otras 4ª
    (370, 4,  'n3_datos_estadisticos_c_epigrafe_iae_otras_4o',                          Char),                  # C -Epígrafe I.A.E. - Otras 4º
    (374, 40, 'n3_datos_estadisticos_a_actividades_otras_5a',                           Char),                  # A - Actividades - Otras - 5ª
    (414, 1,  'n3_datos_estadisticos_b_clave_otras_5a',                                 Number),                # B - Clave - Otras 5ª
    (415, 4,  'n3_datos_estadisticos_c_epigrafe_iae_otras_5o',                          Char),                  # C -Epígrafe I.A.E. - Otras 5º
    (419, 1,  'n3_datos_estadisticos_d_declaracion_anual_operac_con_terceras_personas', Char),                  # D - Declaración anual operac. con terceras personas.
    (420, 9,  'n3_datos_estadisticos_declaracion_conjunta_nif',                         Char),                  # Declaración conjunta - NIF.
    (429, 37, 'n3_datos_estadisticos_declaracion_conjunta_razon_social',                Char),                  # Declaración conjunta - Razón social
    # 4. Representante - Personas Físicas/Comunid. Bienes - Represent.
    (466, 9,  'n4_representante_personas_fisicas_comunid_bienes_represent_nif',                             Char), # NIF.
    (475, 80, 'n4_representante_personas_fisicas_comunid_bienes_represent_apellidos_y_nombre_razon_social', Char), # Apellidos y Nombre/Razón social
    (555, 2,  'n4_representante_personas_fisicas_comunid_bienes_represent_calle_pza_avda',                  Char), # Calle/Pza./Avda.
    (557, 17, 'n4_representante_personas_fisicas_comunid_bienes_represent_nombre_de_la_via_publica',        Char), # Nombre de la vía pública
    (574, 5,  'n4_representante_personas_fisicas_comunid_bienes_represent_numero',                          Char), # Número
    (579, 2,  'n4_representante_personas_fisicas_comunid_bienes_represent_esc',                             Char), # Esc.
    (581, 2,  'n4_representante_personas_fisicas_comunid_bienes_represent_piso',                            Char), # Piso
    (583, 2,  'n4_representante_personas_fisicas_comunid_bienes_represent_prta',                            Char), # Prta.
    (585, 9,  'n4_representante_personas_fisicas_comunid_bienes_represent_telefono',                        Char), # Teléfono
    (594, 20, 'n4_representante_personas_fisicas_comunid_bienes_represent_municipio',                       Char), # Municipio
    (614, 15, 'n4_representante_personas_fisicas_comunid_bienes_represent_provincia',                       Char), # Provincia
    (629, 5,  'n4_representante_personas_fisicas_comunid_bienes_represent_codigo_postal',                   Char), # Código Postal
    # 4. Representante - Personas Jurídicas - Represent.
    (634, 80, 'n4_representante_personas_juridicas_represent_1_nombre_y_apellidos',                         Char),    # 1 - Nombre y Apellidos
    (714, 9,  'n4_representante_personas_juridicas_represent_1_nif',                                        Char),    # 1 - NIF
    (723, 8,  'n4_representante_personas_juridicas_represent_1_fecha_poder_ddmmaaaa',                       Number),  # 1 - Fecha Poder (DDMMAAAA)
    (731, 12, 'n4_representante_personas_juridicas_represent_1_notaria',                                    Char),    # 1 - Notaría
    (743, 80, 'n4_representante_personas_juridicas_represent_2_nombre_y_apellidos',                         Char),    # 2 - Nombre y Apellidos
    (823, 9,  'n4_representante_personas_juridicas_represent_2_nif',                                        Char),    # 2 - NIF
    (832, 8,  'n4_representante_personas_juridicas_represent_2_fecha_poder_ddmmaaaa',                       Number),  # 2 - Fecha Poder (DDMMAAAA)
    (840, 12, 'n4_representante_personas_juridicas_represent_2_notaria',                                    Char),    # 2 - Notaría
    (852, 80, 'n4_representante_personas_juridicas_represent_3_nombre_y_apellidos',                         Char),    # 3 - Nombre y Apellidos
    (932, 9,  'n4_representante_personas_juridicas_represent_3_nif',                                        Char),    # 3 - NIF
    (941, 8,  'n4_representante_personas_juridicas_represent_3_fecha_poder_ddmmaaaa',                       Number),  # 3 - Fecha Poder (DDMMAAAA)
    (949, 12, 'n4_representante_personas_juridicas_represent_3_notaria',                                    Char),    # 3 - Notaría
    (961, 21,   'aeat3',                 Char),                  # RESERVADO PARA LA A.E.A.T. (Dejar en blanco) Incluye Nº Referencia PADRE's
    (982, 13,   'electronic_stamp_aeat', Char),                  # RESERVADO PARA LA A.E.A.T. (Dejar en blanco) Sello electrónico
    (995, 20,   'EEDD',                  Char),                  # Identificador cliente EEDD. RESERVADO PARA LAS EEDD.
    (1015, 150, 'aeat4',                 Char),                  # RESERVADO PARA LA A.E.A.T. (Dejar en blanco)
    (1165, 12,  'record_end',            Const('</T39001000>')), # Indentificador de Fin de registro
)

# Pag. 2
PAGE2_RECORD = (
    (1, 2,  'model_start',         Const('<T')),    # Inicio del identificador de modelo y página.
    (3, 3,  'model',               Const(MODEL)),   # Modelo.
    (6, 5,  'page',                Const('02000')), # Página.
    (11, 1, 'model_end',           Const('>')),     # Fin de identificador de modelo.
    (12, 1, 'complementary_page',  Const(' ')),     # Indicador de página complementaria
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Reg. ordin.
    (13, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_base_imponible_01',                                   Numeric(sign=SIGN_N)),  # Base imponible [01]
    (30, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_cuota_02',                                            Numeric(sign=SIGN_N)),  # Cuota [02]
    (47, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_base_imponible_03',                                   Numeric(sign=SIGN_N)),  # Base imponible [03]
    (64, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_cuota_04',                                            Numeric(sign=SIGN_N)),  # Cuota [04]
    (81, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_base_imponible_05',                                   Numeric(sign=SIGN_N)),  # Base imponible [05]
    (98, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_ordin_cuota_06',                                            Numeric(sign=SIGN_N)),  # Cuota [06]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - operaciones intragrupo
    (115, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_base_imponible_500',                    Numeric(sign=SIGN_N)),  # Base imponible [500]
    (132, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_cuota_501',                             Numeric(sign=SIGN_N)),  # Cuota [501]
    (149, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_base_imponible_502',                    Numeric(sign=SIGN_N)),  # Base imponible [502]
    (166, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_cuota_503',                             Numeric(sign=SIGN_N)),  # Cuota [503]
    (183, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_base_imponible_504',                    Numeric(sign=SIGN_N)),  # Base imponible [504]
    (200, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_operaciones_intragrupo_cuota_505',                             Numeric(sign=SIGN_N)),  # Cuota [505]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - regimen especial criterio caja
    (217, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_base_imponible_643',            Numeric(sign=SIGN_N)),  # Base imponible [643]
    (234, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_cuota_644',                     Numeric(sign=SIGN_N)),  # Cuota [644]
    (251, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_base_imponible_645',            Numeric(sign=SIGN_N)),  # Base imponible [645]
    (268, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_cuota_646',                     Numeric(sign=SIGN_N)),  # Cuota [646]
    (285, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_base_imponible_647',            Numeric(sign=SIGN_N)),  # Base imponible [647]
    (302, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_regimen_especial_criterio_caja_cuota_648',                     Numeric(sign=SIGN_N)),  # Cuota [648]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Reg. espec. bienes usados
    (319, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_base_imponible_07',                    Numeric(sign=SIGN_N)),  # Base imponible [07]
    (336, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_cuota_08',                             Numeric(sign=SIGN_N)),  # Cuota [08]
    (353, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_base_imponible_09',                    Numeric(sign=SIGN_N)),  # Base imponible [09]
    (370, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_cuota_10',                             Numeric(sign=SIGN_N)),  # Cuota [10]
    (387, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_base_imponible_11',                    Numeric(sign=SIGN_N)),  # Base imponible [11]
    (404, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_bienes_usados_cuota_12',                             Numeric(sign=SIGN_N)),  # Cuota [12]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Reg. espec. agencias viajes
    (421, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_agencias_viajes_base_imponible_13',                  Numeric(sign=SIGN_N)),  # Base imponible [13]
    (438, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_reg_espec_agencias_viajes_cuota_14',                           Numeric(sign=SIGN_N)),  # Cuota [14]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Adquis. intracomunit. bienes
    (455, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_base_imponible_21',                 Numeric(sign=SIGN_N)),  # Base imponible [21]
    (472, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_cuota_22',                          Numeric(sign=SIGN_N)),  # Cuota [22]
    (489, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_base_imponible_23',                 Numeric(sign=SIGN_N)),  # Base imponible [23]
    (506, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_cuota_24',                          Numeric(sign=SIGN_N)),  # Cuota [24]
    (523, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_base_imponible_25',                 Numeric(sign=SIGN_N)),  # Base imponible [25]
    (540, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_bienes_cuota_26',                          Numeric(sign=SIGN_N)),  # Cuota [26]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Adquis. intracomunit. servicios
    (557, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_base_imponible_545',             Numeric(sign=SIGN_N)),  # Base Imponible [545]
    (574, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_cuota_546',                      Numeric(sign=SIGN_N)),  # Cuota [546]
    (591, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_base_imponible_547',             Numeric(sign=SIGN_N)),  # Base Imponible [547]
    (608, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_cuota_548',                      Numeric(sign=SIGN_N)),  # Cuota [548]
    (625, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_base_imponible_551',             Numeric(sign=SIGN_N)),  # Base Imponible [551]
    (642, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_adquis_intracomunit_servicios_cuota_552',                      Numeric(sign=SIGN_N)),  # Cuota [552]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - IVA deveng. invers. sujeto pasivo
    (659, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_iva_deveng_invers_sujeto_pasivo_base_imponible_27',            Numeric(sign=SIGN_N)),  # Base imponible [27]
    (676, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_iva_deveng_invers_sujeto_pasivo_cuota_28',                     Numeric(sign=SIGN_N)),  # Cuota [28]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Modificac. bases y cuotas
    (693, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_y_cuotas_base_imponible_29',                   Numeric(sign=SIGN_N)),  # Base imponible [29]
    (710, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_y_cuotas_cuota_30',                            Numeric(sign=SIGN_N)),  # Cuota [30]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Modificac. bases y cuotas intragrupo
    (727, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_y_cuotas_intragrupo_cuota_649',                Numeric(sign=SIGN_N)),  # Base [649]
    (744, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_y_cuotas_intragrupo_cuota_650',                Numeric(sign=SIGN_N)),  # Cuota [650]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Modificac. bases/cuotas concurso acreedores
    (761, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_cuotas_concurso_acreedores_base_imponible_31', Numeric(sign=SIGN_N)),  # Base imponible [31]
    (778, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modificac_bases_cuotas_concurso_acreedores_cuota_32',          Numeric(sign=SIGN_N)),  # Cuota [32]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Total bases y cuotas IVA
    (795, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_total_bases_y_cuotas_iva_base_imponible_33',                   Numeric(sign=SIGN_N)),  # Base imponible [33]
    (812, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_total_bases_y_cuotas_iva_cuota_34',                            Numeric(sign=SIGN_N)),  # Cuota [34]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Recargo de equivalencia
    (829, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_base_imponible_35',                    Numeric(sign=SIGN_N)),  # Base imponible [35]
    (846, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_cuota_36',                             Numeric(sign=SIGN_N)),  # Cuota [36]
    (863, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_base_imponible_599',                   Numeric(sign=SIGN_N)),  # Base imponible [599]
    (880, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_cuota_600',                            Numeric(sign=SIGN_N)),  # Cuota [600]
    (897, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_base_imponible_601',                   Numeric(sign=SIGN_N)),  # Base imponible [601]
    (914, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_cuota_602',                            Numeric(sign=SIGN_N)),  # Cuota [602]
    (931, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_base_imponible_41',                    Numeric(sign=SIGN_N)),  # Base imponible [41]
    (948, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_recargo_de_equivalencia_cuota_42',                             Numeric(sign=SIGN_N)),  # Cuota [42]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Modific. recargo equivalencia
    (965, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modific_recargo_equivalencia_base_imponible_43',               Numeric(sign=SIGN_N)),  # Base imponible [43]
    (982, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modific_recargo_equivalencia_cuota_44',                        Numeric(sign=SIGN_N)),  # Cuota [44]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota - Modific. recargo equiv. Concurso acreedores
    (999, 17,'n5_operaciones_reg_gral_base_imponible_y_cuota_modific_recargo_equiv_concurso_acreedores_base_imponible_45',  Numeric(sign=SIGN_N)),  # Base imponible [45]
    (1016, 17, 'n5_operaciones_reg_gral_base_imponible_y_cuota_modific_recargo_equiv_concurso_acreedores_cuota_46',         Numeric(sign=SIGN_N)),  # Cuota [46]
    # 5. Operaciones Reg. Gral. - Base Imponible y cuota
    (1033, 17, 'n5_operaciones_reg_gral_base_imponible_y_cuota_total_cuotas_iva_y_recargo_equivalencia_47',                 Numeric(sign=SIGN_N)),  # Total cuotas IVA y recargo equivalencia [47]
    (1050, 150, 'aeat', Char),
    (1200, 12, 'record_end', Const('</T39002000>')),
)

# Pag. 3
PAGE3_RECORD = (
    (1, 2, 'model_start',          Const('<T')),    # Inicio del identificador de modelo y página.
    (3, 3, 'model',                Const(MODEL)),   # Modelo.
    (6, 5, 'page',                 Const('03000')), # Página.
    (11, 1, 'model_end',           Const('>')),     # Fin de identificador de modelo.
    (12, 1, 'complementary_page',  Const(' ')),     # Indicador de página complementaria
    # 5. Operaciones Reg. Gral. - IVA deducible - Oper. inter. corrientes bienes y servic.
    (13, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_190',       Numeric(sign=SIGN_N)), # Base imponible [190]
    (30, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_191',                Numeric(sign=SIGN_N)), # Cuota [191]
    (47, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_192',       Numeric(sign=SIGN_N)), # Base imponible [192]
    (64, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_193',                Numeric(sign=SIGN_N)), # Cuota [193]
    (81, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_555',       Numeric(sign=SIGN_N)), # Base imponible [555]
    (98, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_556',                Numeric(sign=SIGN_N)), # Cuota [556]
    (115, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_603',      Numeric(sign=SIGN_N)), # Base imponible [603]
    (132, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_604',               Numeric(sign=SIGN_N)), # Cuota [604]
    (149, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_194',      Numeric(sign=SIGN_N)), # Base imponible [194]
    (166, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_195',               Numeric(sign=SIGN_N)), # Cuota [195]
    (183, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_557',      Numeric(sign=SIGN_N)), # Base imponible [557]
    (200, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_558',               Numeric(sign=SIGN_N)), # Cuota [558]
    (217, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_base_imponible_605',      Numeric(sign=SIGN_N)), # Base imponible [605]
    (234, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_corrientes_bienes_y_servic_cuota_606',               Numeric(sign=SIGN_N)), # Cuota [606]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total oper. inter. corrientes bienes y servic. 
    (251, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_inter_corrientes_bienes_y_servic_base_imponible_48', Numeric(sign=SIGN_N)), # Base imponible [48]
    (268, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_inter_corrientes_bienes_y_servic_cuota_49',          Numeric(sign=SIGN_N)), # Cuota [49]
    # 5. Operaciones Reg. Gral. - IVA deducible - Oper. intragrupo corrientes 
    (285, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_506',                 Numeric(sign=SIGN_N)), # Base imponible [506]
    (302, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_507',                          Numeric(sign=SIGN_N)), # Cuota [507]
    (319, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_508',                 Numeric(sign=SIGN_N)), # Base imponible [508]
    (336, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_509',                          Numeric(sign=SIGN_N)), # Cuota [509]
    (353, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_559',                 Numeric(sign=SIGN_N)), # Base imponible [559]
    (370, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_560',                          Numeric(sign=SIGN_N)), # Cuota [560]
    (387, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_607',                 Numeric(sign=SIGN_N)), # Base imponible [607]
    (404, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_608',                          Numeric(sign=SIGN_N)), # Cuota [608]
    (421, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_510',                 Numeric(sign=SIGN_N)), # Base imponible [510]
    (438, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_511',                          Numeric(sign=SIGN_N)), # Cuota [511]
    (455, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_561',                 Numeric(sign=SIGN_N)), # Base imponible [561]
    (472, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_562',                          Numeric(sign=SIGN_N)), # Cuota [562]
    (489, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_base_imponible_609',                 Numeric(sign=SIGN_N)), # Base imponible [609]
    (506, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intragrupo_corrientes_cuota_610',                          Numeric(sign=SIGN_N)), # Cuota [610]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total oper. intragrupo corrientes 
    (523, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_intragrupo_corrientes_base_imponible_512',           Numeric(sign=SIGN_N)), # Base imponible [512]
    (540, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_intragrupo_corrientes_cuota_513',                    Numeric(sign=SIGN_N)), # Cuota [513]
    # 5. Operaciones Reg. Gral. - IVA deducible - Oper. inter. bienes de inversion
    (557, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_196',             Numeric(sign=SIGN_N)), # Base imponible [196]
    (574, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_197',                      Numeric(sign=SIGN_N)), # Cuota [197]
    (591, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_198',             Numeric(sign=SIGN_N)), # Base imponible [198]
    (608, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_199',                      Numeric(sign=SIGN_N)), # Cuota [199]
    (625, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_563',             Numeric(sign=SIGN_N)), # Base imponible [563]
    (642, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_564',                      Numeric(sign=SIGN_N)), # Cuota [564]
    (659, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_611',             Numeric(sign=SIGN_N)), # Base imponible [611]
    (676, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_612',                      Numeric(sign=SIGN_N)), # Cuota [612]
    (693, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_200',             Numeric(sign=SIGN_N)), # Base imponible [200]
    (710, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_201',                      Numeric(sign=SIGN_N)), # Cuota [201]
    (727, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_565',             Numeric(sign=SIGN_N)), # Base imponible [565]
    (744, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_566',                      Numeric(sign=SIGN_N)), # Cuota [566]
    (761, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_base_imponible_613',             Numeric(sign=SIGN_N)), # Base imponible [613]
    (778, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_inter_bienes_de_inversion_cuota_614',                      Numeric(sign=SIGN_N)), # Cuota [614]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total oper. inter. bienes de inversion
    (795, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_inter_bienes_de_inversion_base_imponible_50',        Numeric(sign=SIGN_N)), # Base imponible [50]
    (812, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_inter_bienes_de_inversion_cuota_51',                 Numeric(sign=SIGN_N)), # Cuota [51]
    # 5. Operaciones Reg. Gral. - IVA deducible - Oper. intra. bienes de inversion
    (829, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_514',             Numeric(sign=SIGN_N)), # Base imponible [514]
    (846, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_515',                      Numeric(sign=SIGN_N)), # Cuota [515]
    (863, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_516',             Numeric(sign=SIGN_N)), # Base imponible [516]
    (880, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_517',                      Numeric(sign=SIGN_N)), # Cuota [517]
    (897, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_567',             Numeric(sign=SIGN_N)), # Base imponible [567]
    (914, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_568',                      Numeric(sign=SIGN_N)), # Cuota [568]
    (931, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_615',             Numeric(sign=SIGN_N)), # Base imponible [615]
    (948, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_616',                      Numeric(sign=SIGN_N)), # Cuota [616]
    (965, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_518',             Numeric(sign=SIGN_N)), # Base imponible [518]
    (982, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_519',                      Numeric(sign=SIGN_N)), # Cuota [519]
    (999, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_569',             Numeric(sign=SIGN_N)), # Base imponible [569]
    (1016, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_570',                     Numeric(sign=SIGN_N)), # Cuota [570]
    (1033, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_base_imponible_617',            Numeric(sign=SIGN_N)), # Base imponible [617]
    (1050, 17, 'n5_operaciones_reg_gral_iva_deducible_oper_intra_bienes_de_inversion_cuota_618',                     Numeric(sign=SIGN_N)), # Cuota [618]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total oper. intra. bienes de inversion
    (1067, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_intra_bienes_de_inversion_base_imponible_520',      Numeric(sign=SIGN_N)), # Base imponible [520]
    (1084, 17, 'n5_operaciones_reg_gral_iva_deducible_total_oper_intra_bienes_de_inversion_cuota_521',               Numeric(sign=SIGN_N)), # Cuota [521]
    # 5. Operaciones Reg. Gral. - IVA deducible - Import. bienes corrientes 
    (1101, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_202',                  Numeric(sign=SIGN_N)), # Base imponible [202]
    (1118, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_203',                           Numeric(sign=SIGN_N)), # Cuota [203]
    (1135, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_204',                  Numeric(sign=SIGN_N)), # Base imponible [204]
    (1152, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_205',                           Numeric(sign=SIGN_N)), # Cuota [205]
    (1169, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_571',                  Numeric(sign=SIGN_N)), # Base imponible [571]
    (1186, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_572',                           Numeric(sign=SIGN_N)), # Cuota [572]
    (1203, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_619',                  Numeric(sign=SIGN_N)), # Base imponible [619]
    (1220, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_620',                           Numeric(sign=SIGN_N)), # Cuota [620]
    (1237, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_206',                  Numeric(sign=SIGN_N)), # Base imponible [206]
    (1254, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_207',                           Numeric(sign=SIGN_N)), # Cuota [207]
    (1271, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_573',                  Numeric(sign=SIGN_N)), # Base imponible [573]
    (1288, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_574',                           Numeric(sign=SIGN_N)), # Cuota [574]
    (1305, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_base_imponible_621',                  Numeric(sign=SIGN_N)), # Base imponible [621]
    (1322, 17, 'n5_operaciones_reg_gral_iva_deducible_import_bienes_corrientes_cuota_622',                           Numeric(sign=SIGN_N)), # Cuota [622]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total import. bienes corrientes 
    (1339, 17, 'n5_operaciones_reg_gral_iva_deducible_total_import_bienes_corrientes_base_imponible_52',             Numeric(sign=SIGN_N)), # Base imponible [52]
    (1356, 17, 'n5_operaciones_reg_gral_iva_deducible_total_import_bienes_corrientes_cuota_53',                      Numeric(sign=SIGN_N)), # Cuota [53]

    (1373, 150, 'aeat', Char),
    (1523, 12, 'record_end', Const('</T39003000>'))
)

# Pag. 4
PAGE4_RECORD = (
    (1, 2,  'model_start',         Const('<T')),    # Inicio del identificador de modelo
    (3, 3,  'model',               Const(MODEL)),   # Modelo.
    (6, 5,  'page',                Const('04000')), # Página.
    (11, 1, 'model_end',          Const('>')),      # Fin de identificador de modelo.
    (12, 1, 'complementary_page', Const(' ')),      # Indicador de página complementaria
    # 5. Operaciones Reg. Gral. - IVA deducible - Importacion bienes inversion
    (13, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_208',                       Numeric(sign=SIGN_N)), # Base imponible [208]
    (30, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_209',                                Numeric(sign=SIGN_N)), # Cuota [209]
    (47, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_210',                       Numeric(sign=SIGN_N)), # Base imponible [210]
    (64, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_211',                                Numeric(sign=SIGN_N)), # Cuota [211]
    (81, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_575',                       Numeric(sign=SIGN_N)), # Base imponible [575]
    (98, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_576',                                Numeric(sign=SIGN_N)), # Cuota [576]
    (115, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_623',                      Numeric(sign=SIGN_N)), # Base imponible [623]
    (132, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_624',                               Numeric(sign=SIGN_N)), # Cuota [624]
    (149, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_212',                      Numeric(sign=SIGN_N)), # Base imponible [212]
    (166, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_213',                               Numeric(sign=SIGN_N)), # Cuota [213]
    (183, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_577',                      Numeric(sign=SIGN_N)), # Base imponible [577]
    (200, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_578',                               Numeric(sign=SIGN_N)), # Cuota [578]
    (217, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_base_imponible_625',                      Numeric(sign=SIGN_N)), # Base imponible [625]
    (234, 17, 'n5_operaciones_reg_gral_iva_deducible_importacion_bienes_inversion_cuota_626',                               Numeric(sign=SIGN_N)), # Cuota [626]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total importacion bienes inversion
    (251, 17, 'n5_operaciones_reg_gral_iva_deducible_total_importacion_bienes_inversion_base_imponible_54',                 Numeric(sign=SIGN_N)), # Base imponible [54]
    (268, 17, 'n5_operaciones_reg_gral_iva_deducible_total_importacion_bienes_inversion_cuota_55',                          Numeric(sign=SIGN_N)), # Cuota [55]
    # 5. Operaciones Reg. Gral. - IVA deducible - Adqui. intra. bienes corrientes
    (285, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_214',                     Numeric(sign=SIGN_N)), # Base imponible [214]
    (302, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_215',                              Numeric(sign=SIGN_N)), # Cuota [215]
    (319, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_216',                     Numeric(sign=SIGN_N)), # Base imponible [216]
    (336, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_217',                              Numeric(sign=SIGN_N)), # Cuota [217]
    (353, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_579',                     Numeric(sign=SIGN_N)), # Base imponible [579]
    (370, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_580',                              Numeric(sign=SIGN_N)), # Cuota [580]
    (387, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_627',                     Numeric(sign=SIGN_N)), # Base imponible [627]
    (404, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_628',                              Numeric(sign=SIGN_N)), # Cuota [628]
    (421, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_218',                     Numeric(sign=SIGN_N)), # Base imponible [218]
    (438, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_219',                              Numeric(sign=SIGN_N)), # Cuota [219]
    (455, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_581',                     Numeric(sign=SIGN_N)), # Base imponible [581]
    (472, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_582',                              Numeric(sign=SIGN_N)), # Cuota [582]
    (489, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_base_imponible_629',                     Numeric(sign=SIGN_N)), # Base imponible [629]
    (506, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_corrientes_cuota_630',                              Numeric(sign=SIGN_N)), # Cuota [630]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total adqui. intra. bienes corrientes
    (523, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_bienes_corrientes_base_imponible_56',                Numeric(sign=SIGN_N)), # Base imponible [56]
    (540, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_bienes_corrientes_cuota_57',                         Numeric(sign=SIGN_N)), # Cuota [57]
    # 5. Operaciones Reg. Gral. - IVA deducible - Adqui. intra. bienes inversion
    (557, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_220',                      Numeric(sign=SIGN_N)), # Base imponible [220]
    (574, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_221',                               Numeric(sign=SIGN_N)), # Cuota [221]
    (591, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_222',                      Numeric(sign=SIGN_N)), # Base imponible [222]
    (608, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_223',                               Numeric(sign=SIGN_N)), # Cuota [223]
    (625, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_583',                      Numeric(sign=SIGN_N)), # Base imponible [583]
    (642, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_584',                               Numeric(sign=SIGN_N)), # Cuota [584]
    (659, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_631',                      Numeric(sign=SIGN_N)), # Base imponible [631]
    (676, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_632',                               Numeric(sign=SIGN_N)), # Cuota [632]
    (693, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_224',                      Numeric(sign=SIGN_N)), # Base imponible [224]
    (710, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_225',                               Numeric(sign=SIGN_N)), # Cuota [225]
    (727, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_585',                      Numeric(sign=SIGN_N)), # Base imponible [585]
    (744, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_586',                               Numeric(sign=SIGN_N)), # Cuota [586]
    (761, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_base_imponible_633',                      Numeric(sign=SIGN_N)), # Base imponible [633]
    (778, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_bienes_inversion_cuota_634',                               Numeric(sign=SIGN_N)), # Cuota [634]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total adqui. intra. bienes inversion
    (795, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_bienes_inversion_base_imponible_58',                 Numeric(sign=SIGN_N)), # Base imponible [58]
    (812, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_bienes_inversion_cuota_59',                          Numeric(sign=SIGN_N)), # Cuota [59]
    # 5. Operaciones Reg. Gral. - IVA deducible - Adqui. intra. servicios
    (829, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_587',                             Numeric(sign=SIGN_N)), # Base imponible [587]
    (846, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_588',                                      Numeric(sign=SIGN_N)), # Cuota [588]
    (863, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_589',                             Numeric(sign=SIGN_N)), # Base imponible [589]
    (880, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_590',                                      Numeric(sign=SIGN_N)), # Cuota [590]
    (897, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_591',                             Numeric(sign=SIGN_N)), # Base imponible [591]
    (914, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_592',                                      Numeric(sign=SIGN_N)), # Cuota [592]
    (931, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_635',                             Numeric(sign=SIGN_N)), # Base imponible [635]
    (948, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_636',                                      Numeric(sign=SIGN_N)), # Cuota [636]
    (965, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_593',                             Numeric(sign=SIGN_N)), # Base imponible [593]
    (982, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_594',                                      Numeric(sign=SIGN_N)), # Cuota [594]
    (999, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_595',                             Numeric(sign=SIGN_N)), # Base imponible [595]
    (1016, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_596',                                     Numeric(sign=SIGN_N)), # Cuota [596]
    (1033, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_base_imponible_637',                            Numeric(sign=SIGN_N)), # Base imponible [637]
    (1050, 17, 'n5_operaciones_reg_gral_iva_deducible_adqui_intra_servicios_cuota_638',                                     Numeric(sign=SIGN_N)), # Cuota [638]
    # 5. Operaciones Reg. Gral. - IVA deducible - Total adqui. intra. servicios
    (1067, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_servicios_base_imponible_597',                      Numeric(sign=SIGN_N)), # Base imponible [597]
    (1084, 17, 'n5_operaciones_reg_gral_iva_deducible_total_adqui_intra_servicios_cuota_598',                               Numeric(sign=SIGN_N)), # Cuota [598]
    # 5. Operaciones Reg. Gral. - IVA deducible - Compensac. rég. especial agric./ganad./pesca
    (1101, 17, 'n5_operaciones_reg_gral_iva_deducible_compensac_reg_especial_agric_ganad_pesca_base_impon_60',              Numeric(sign=SIGN_N)), # Base impon. [60]
    (1118, 17, 'n5_operaciones_reg_gral_iva_deducible_compensac_reg_especial_agric_ganad_pesca_cuota_deduc_61',             Numeric(sign=SIGN_N)), # Cuota deduc. [61]
    # 5. Operaciones Reg. Gral. - IVA deducible - Rectificación de deducciones
    (1135, 17, 'n5_operaciones_reg_gral_iva_deducible_rectificacion_de_deducciones_base_imponible_639',                     Numeric(sign=SIGN_N)), # Base imponible [639]
    (1152, 17, 'n5_operaciones_reg_gral_iva_deducible_rectificacion_de_deducciones_cuota_62',                               Numeric(sign=SIGN_N)), # Cuota [62]
    # 5. Operaciones Reg. Gral. - IVA deducible - Rectificación de deducciones intragrupo
    (1169, 17, 'n5_operaciones_reg_gral_iva_deducible_rectificacion_de_deducciones_intragrupo_base_impon_651',              Numeric(sign=SIGN_N)), # Base impon. [651]
    (1186, 17, 'n5_operaciones_reg_gral_iva_deducible_rectificacion_de_deducciones_intragrupo_cuota_652',                   Numeric(sign=SIGN_N)), # Cuota [652]
    # 5. Operaciones Reg. Gral. - IVA deducible
    (1203, 17, 'n5_operaciones_reg_gral_iva_deducible_regularizacion_de_inversiones_63',                                    Numeric(sign=SIGN_N)), # Regularización de inversiones [63]
    (1220, 17, 'n5_operaciones_reg_gral_iva_deducible_regularizacion_por_aplicacion_porcentaje_definitivo_de_prorrata_522', Numeric(sign=SIGN_N)), # Regularización por aplicación porcentaje definitivo de prorrata [522]
    (1237, 17, 'n5_operaciones_reg_gral_iva_deducible_suma_de_deducciones_64',                                              Numeric(sign=SIGN_N)), # Suma de deducciones [64]
    # 5. Operaciones Reg. Gral.
    (1254, 17, 'n5_operaciones_reg_gral_resultado_regimen_general_65',                                                      Numeric(sign=SIGN_N)), # Resultado régimen general [65]

    (1271, 150, 'aeat', Char),
    (1421, 12, 'record_end', Const('</T39004000>'))
)

# Pag. 5
PAGE5_RECORD = (

)

# Pag. 6
PAGE6_RECORD = (
    (1, 2,  'model_start',         Const('<T')),  # Inicio del identificador de modelo
    (3, 3,  'model',               Const(MODEL)),  # Modelo.
    (6, 5,  'page',                Const('06000')),  # Página.
    (11, 1, 'model_end',           Const('>')),  # Fin de identificador de modelo.
    (12, 1, 'complementary_page',  Const(' ')),  # Indicador de página complementaria
    # 7. Resultado liquidación anual 
    (13, 17, 'n7_resultado_liquidacion_anual_regularizacion_cuotas_art_80cinco5a_liva_658',                                                     Numeric(sign=SIGN_N)),  # Regularización cuotas art. 80.Cinco.5ª LIVA [658]
    (30, 17, 'n7_resultado_liquidacion_anual_suma_de_resultados_84',                                                                            Numeric(sign=SIGN_N)),  # Suma de resultados [84]
    (47, 17, 'n7_resultado_liquidacion_anual_iva_a_la_importacion_liquidado_por_la_aduana_solo_sujetos_pasivos_con_opcion_de_diferimiento_659', Numeric(sign=SIGN_N)),  # IVA a la importación liquidado por la Aduana (sólo sujetos pasivos con opción de diferimiento) [659]
    (64, 17, 'n7_resultado_liquidacion_anual_compensacion_de_cuotas_ejercicio_anterior_85',                                                     Numeric(sign=SIGN_N)),  # Compensación de cuotas ejercicio anterior [85]
    (81, 17, 'n7_resultado_liquidacion_anual_resultado_de_la_liquidacion_86',                                                                   Numeric(sign=SIGN_N)),  # Resultado de la liquidación [86]
    # 8. Tributación razón de territorio - Administraciones
    (98, 5,   'n8_tributacion_razon_de_territorio_administraciones_territorio_comun_87',                                                                             Numeric(sign=WITHOUT_SIGN)),  # Territorio común [87]
    (103, 5,  'n8_tributacion_razon_de_territorio_administraciones_alava_88',                                                                                        Numeric(sign=WITHOUT_SIGN)),  # Álava [88]
    (108, 5,  'n8_tributacion_razon_de_territorio_administraciones_guipuzcoa_89',                                                                                    Numeric(sign=WITHOUT_SIGN)),  # Guipúzcoa [89]
    (113, 5,  'n8_tributacion_razon_de_territorio_administraciones_vizcaya_90',                                                                                      Numeric(sign=WITHOUT_SIGN)),  # Vizcaya [90]
    (118, 5,  'n8_tributacion_razon_de_territorio_administraciones_navarra_91',                                                                                      Numeric(sign=WITHOUT_SIGN)),  # Navarra [91]
    (123, 17, 'n8_tributacion_razon_de_territorio_administraciones_regularizacion_cuotas_art_80cinco5a_liva_658',                                                    Numeric(sign=SIGN_N)),  # Regularización cuotas art. 80.Cinco.5ª LIVA [658]
    (140, 17, 'n8_tributacion_razon_de_territorio_administraciones_suma_de_resultados_84',                                                                           Numeric(sign=SIGN_N)),  # Suma de resultados [84]
    (157, 17, 'n8_tributacion_razon_de_territorio_administraciones_resultado_atribuible_a_territorio_comun_92',                                                      Numeric(sign=SIGN_N)),  # Resultado atribuible a territorio común [92]
    (174, 17, 'n8_tributacion_razon_de_territorio_administraciones_iva_a_la_importacion_liquidado_por_la_aduanasolo_sujetos_pasivos_con_opcion_de_diferimiento_659', Numeric(sign=SIGN_N)),  # IVA a la importación liquidado por la Aduana(sólo sujetos pasivos con opción de diferimiento) [659]
    (191, 17, 'n8_tributacion_razon_de_territorio_administraciones_compens_cuotas_ej_anterior_atrib_territ_com_93',                                                  Numeric(sign=SIGN_N)),  # Compens. cuotas ej. anterior atrib. territ. com. [93]
    (208, 17, 'n8_tributacion_razon_de_territorio_administraciones_resultado_liq_anual_atribuible_territ_comun_94',                                                  Numeric(sign=SIGN_N)),  # Resultado liq. anual atribuible territ. comun [94]
    # 9. Resultado de las liquidaciones 
    (225, 17, 'n9_resultado_de_las_liquidaciones_total_resultados_a_ingresar_autoliquidaciones_de_iva_del_ejercicio_95',     Numeric(sign=SIGN_N)),  # Total resultados a ingresar autoliquidaciones de IVA del ejercicio [95]
    (242, 17, 'n9_resultado_de_las_liquidaciones_total_devoluc_mensuales_iva_suj_pasivos_regtro_de_devolucion_mensual_96',   Numeric(sign=SIGN_N)),  # Total devoluc. mensuales IVA suj. pasivos Regtro. de devolución mensual [96]
    (259, 17, 'n9_resultado_de_las_liquidaciones_total_devoluc_por_cuotas_en_la_adquisicion_de_elementos_de_transporte_524', Numeric(sign=SIGN_N)),  # Total devoluc. Por cuotas en la adquisicion de elementos de transporte [524]
    (276, 17, 'n9_resultado_de_las_liquidaciones_resultado_declaracionliquidacion_ultimo_periodo_a_compensar_97',            Numeric(sign=SIGN_N)),  # Resultado declaración-liquidación último periodo - A compensar [97]
    (293, 17, 'n9_resultado_de_las_liquidaciones_resultado_declaracionliquidacion_ultimo_periodo_a_devolver_98',             Numeric(sign=SIGN_N)),  # Resultado declaración-liquidación último periodo - A devolver [98]
    (310, 17, 'n9_resultado_de_las_liquidaciones_total_resultados_positivos_del_ejercicio_modelo_322_525',                   Numeric(sign=SIGN_N)),  # Total resultados positivos del ejercicio (modelo 322) [525]
    (327, 17, 'n9_resultado_de_las_liquidaciones_total_resultados_negativos_del_ejercicio_modelo_322_526',                   Numeric(sign=SIGN_N)),  # Total resultados negativos del ejercicio (modelo 322) [526]
    # 10. Volumen de operaciones 
    (344, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_general_99',                                                                        Numeric(sign=SIGN_N)),  # Operaciones en régimen general [99]
    (361, 17, 'n10_volumen_de_operaciones_operaciones_regimen_especial_del_criterio_de_caja_653',                                                    Numeric(sign=SIGN_N)),  # Operaciones régimen especial del criterio de caja [653]
    (378, 17, 'n10_volumen_de_operaciones_entregas_intracomunitarias_exentas_103',                                                                   Numeric(sign=SIGN_N)),  # Entregas intracomunitarias exentas [103]
    (395, 17, 'n10_volumen_de_operaciones_exportaciones_y_otras_operaciones_exentas_con_derecho_a_deduccion_104',                                    Numeric(sign=SIGN_N)),  # Exportaciones y otras operaciones exentas con derecho a deducción [104]
    (412, 17, 'n10_volumen_de_operaciones_operaciones_exentas_sin_derecho_a_deduccion_105',                                                          Numeric(sign=SIGN_N)),  # Operaciones exentas sin derecho a deducción [105]
    (429, 17, 'n10_volumen_de_operaciones_operaciones_no_sujetas_o_con_inversion_de_suj_pasivo_110',                                                 Numeric(sign=SIGN_N)),  # Operaciones no sujetas o con inversion de suj. Pasivo [110]
    (446, 17, 'n10_volumen_de_operaciones_entregas_de_bienes_objeto_de_instalacion_o_montaje_en_otros_estados_miembros_112',                         Numeric(sign=SIGN_N)),  # Entregas de bienes objeto de instalación o montaje en otros Estados miembros [112]
    (463, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_simplificado_100',                                                                  Numeric(sign=SIGN_N)),  # Operaciones en régimen simplificado [100]
    (480, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_especias_de_la_agricultura_ganaderia_y_pesca_101',                                  Numeric(sign=SIGN_N)),  # Operaciones en régimen especias de la agricultura, ganadería y pesca [101]
    (497, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_especial_del_recargo_de_equivalencia_102',                                          Numeric(sign=SIGN_N)),  # Operaciones en régimen especial del recargo de equivalencia [102].
    (514, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_especias_de_bienes_usados_objetos_de_arte_antiguedades_y_objetos_de_coleccion_227', Numeric(sign=SIGN_N)),  # Operaciones en régimen especias de bienes usados, objetos de arte, antigüedades y objetos de colección [227].
    (531, 17, 'n10_volumen_de_operaciones_operaciones_en_regimen_especial_de_agencias_de_viajes_228',                                                Numeric(sign=SIGN_N)),  # Operaciones en régimen especial de agencias de viajes [228].
    (548, 17, 'n10_volumen_de_operaciones_entrega_de_bienes_inmuebles_y_operaciones_financieras_no_habituales_106',                                  Numeric(sign=SIGN_N)),  # Entrega de bienes inmuebles y operaciones financieras no habituales [106]
    (565, 17, 'n10_volumen_de_operaciones_entrega_de_bienes_de_inversion_107',                                                                       Numeric(sign=SIGN_N)),  # Entrega de bienes de inversion [107]
    (582, 17, 'n10_volumen_de_operaciones_total_volumen_de_operaciones_108',                                                                         Numeric(sign=SIGN_N)),  # Total volumen de operaciones [108]

    (599, 150, 'aeat', Char),
    (749, 12, 'record_end', Const('</T39006000>'))
)

# Pag. 7
PAGE7_RECORD = (
    (1,  2, 'model_start',         Const('<T')),     # Inicio del identificador de modelo
    (3,  3, 'model',               Const(MODEL)),    # Modelo.
    (6,  5, 'page',                Const('06000')),  # Página.
    (11, 1, 'model_end',           Const('>')),      # Fin de identificador de modelo.
    (12, 1, 'complementary_page',  Const(' ')),      # Indicador de página complementaria
    # 11. Oper. Específicas
    (13,  17, 'field_230',         Numeric(sign=SIGN_N)), # Adquisiciones interiores exentas [230]
    (30,  17, 'field_109',         Numeric(sign=SIGN_N)), # Adquisiciones intracomunitarias exentas [109]
    (47,  17, 'field_231',         Numeric(sign=SIGN_N)), # Importaciones exentas [231]
    (64,  17, 'field_232',         Numeric(sign=SIGN_N)), # Bases imponibles IVA soportado no deducible [232]
    (81,  17, 'field_111',         Numeric(sign=SIGN_N)), # Oper. sujetas que originan el derecho a la devolución mensual [111]
    (98,  17, 'field_113',         Numeric(sign=SIGN_N)), # Entrega interior bienes devengada por invers. sujet. pasiv. operac. triangul. [113]
    (115, 17, 'field_523',         Numeric(sign=SIGN_N)), # Servicios localizados en el territorio de aplicación del impuesto por inversión del sujeto pasivo [523]
    (132, 17, 'field_654',         Numeric(sign=SIGN_N)), # Importes de las entregas de bienes regimen especial criterio caja - Base imponible [654]
    (149, 17, 'field_655',         Numeric(sign=SIGN_N)), # Importes de las entregas de bienes regimen especial criterio caja - Cuota [655]
    (166, 17, 'field_656',         Numeric(sign=SIGN_N)), # Importes de las adquisiciones de bienes regimen especial criterio caja - Base imponible [656]
    (183, 14, 'field_657',         Numeric(sign=SIGN_N)), # Importes de las adquisiciones de bienes regimen especial criterio caja - Cuota [657]
    # 12. Prorratas
    (200, 40, 'pror_activity_1',   Char),                       # Actividad desarrollada
    (240, 3,  'field_114_1',       Char),                       # Código CNAE [114]
    (243, 17, 'field_115_1',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones [115]
    (260, 17, 'field_116_1',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones con derecho a deducción [116]
    (277, 1,  'field_117_1',       Char),                       # Tipo de prorrata [117]
    (278, 5,  'field_118_1',       Numeric(sign=WITHOUT_SIGN)), # % de prorrata [118]
    (283, 40, 'pror_activity_2',   Char),                       # Actividad desarrollada
    (323, 3,  'field_114_2',       Char),                       # Código CNAE [114]
    (326, 17, 'field_115_2',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones [115]
    (343, 17, 'field_116_2',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones con derecho a deducción [116]
    (360, 1,  'field_117_2',       Char),                       # Tipo de prorrata [117]
    (361, 5,  'field_118_2',       Numeric(sign=WITHOUT_SIGN)), # % de prorrata [118]
    (366, 40, 'pror_activity_3',   Char),                       # Actividad desarrollada
    (406, 3,  'field_114_3',       Char),                       # Código CNAE [114]
    (409, 17, 'field_115_3',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones [115]
    (426, 17, 'field_116_3',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones con derecho a deducción [116]
    (443, 1,  'field_117_3',       Char),                       # Tipo de prorrata [117]
    (444, 5,  'field_118_3',       Numeric(sign=WITHOUT_SIGN)), # % de prorrata [118]
    (449, 40, 'pror_activity_4',   Char),                       # Actividad desarrollada
    (489, 3,  'field_114_4',       Char),                       # Código CNAE [114]
    (492, 17, 'field_115_4',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones [115]
    (509, 17, 'field_116_4',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones con derecho a deducción [116]
    (526, 1,  'field_117_4',       Char),                       # Tipo de prorrata [117]
    (527, 5,  'field_118_4',       Numeric(sign=WITHOUT_SIGN)), # % de prorrata [118]
    (532, 40, 'pror_activity_5',   Char),                       # Actividad desarrollada
    (572, 3,  'field_114_5',       Char),                       # Código CNAE [114]
    (575, 17, 'field_115_5',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones [115]
    (592, 17, 'field_116_5',       Numeric(sign=WITHOUT_SIGN)), # Importe de operaciones con derecho a deducción [116]
    (609, 1,  'field_117_5',       Char),                       # Tipo de prorrata [117]
    (610, 5,  'field_118_5',       Numeric(sign=WITHOUT_SIGN)), # % de prorrata [118]

    (615, 150, 'aeat', Char),
    (765, 12, 'record_end', Const('</T39006000>'))
)

# Pag. 8
PAGE8_RECORD = (

)

RECORDS = (PAGE1_RECORD, PAGE2_RECORD, PAGE3_RECORD, PAGE4_RECORD,
           PAGE5_RECORD, PAGE6_RECORD, PAGE7_RECORD, PAGE8_RECORD, )
