# -*- coding: utf-8 -*-

from retrofix.fields import Const, Number, Char, Integer, Numeric, BOOLEAN_X, Boolean, WITHOUT_SIGN

"""
Diseño de registro del modelo 115 (v 1.2)
-----------------------------------------------------------------------------------------------------
115 - Orden EHA/3435/2007 (Ejercicios 2015 y siguientes)

Retenciones e ingresos a cuenta. 
Rentas o rendimientos procedentes del arrendamiento o subarrendamiento de inmuebles urbanos.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '115'

# DR 11500
HEADER_RECORD = (
    (1,   2,   'open_tag',                      Const('<T')),         # Constante.
    (3,   3,   'model',                         Const(MODEL)),        # Modelo
    (6,   1,   'page',                          Const('0')),          # Constante.
    (7,   4,   'fiscalyear',                    Number),              # Ejercicio devengo. (AAAA)
    (11,  2,   'period',                        Char),                # Período. (PP)
    (13,  5,   'close_tag',                     Const('0000>')),      # Constante
    (18,  5,   'open_aux',                      Const('<AUX>')),      # Constante
    (23,  70,  'aeat_1',                        Char),                # Reservado para la Administración. Rellenar con blancos
    (93,  4,   'program_version',               Char),                # Versión del Programa (Nota 1)
    (97,  4,   'aeat_2',                        Char),                # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat',               Char),                # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',                        Char),                # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',                     Const('</AUX>'))      # Constante
)

FOOTER_RECORD = (
    (1, 3, 'open_tag',                          Const('</T')),        # Constante.
    (4, 3, 'model',                             Const(MODEL)),        # Modelo
    (7, 1, 'page',                              Const('0')),          # Constante.
    (8, 4, 'fiscalyear',                        Number),              # Ejercicio devengo. (AAAA)
    (12, 2, 'period',                           Char),                # Período. (PP)
    (14, 5, 'close_tag',                        Const('0000>')),      # Constante
)

# DR 11501
RECORD = (
    (1,   2,   'record_start',                  Const('<T')),  # Inicio del identificador de modelo y página
    (3,   3,   'model',                         Const(MODEL)),  # Modelo
    (6,   2,   'page',                          Const('01')),  # Página
    (8,   4,   'model_end',                     Const('000>')),  # Fin de identificador de modelo
    (12,  1,   'complementary_page',            Char),  # Reservado página complementaria
    (13,  1,   'declaration_type',              Char),  # Tipo de declaración
    # Identificación.
    (14,  9,   'nif',                           Char),  # Identificación. Sujeto pasivo. NIF
    (23,  60,  'subject_surname',               Char),  # Identificación. Sujeto pasivo. Razón o denominación social/Apellidos
    (83,  20,  'subject_name',                  Char),  # Identificación. Sujeto pasivo. Nombre
    (103, 4,   'fiscalyear',                    Number),  # Identificación. Ejercicio
    (107, 2,   'period',                        Char),  # Identificación. Periodo
    # Retenciones e ingresos a cuenta.
    (109, 15,  'number_receivers',              Integer),  # Número perceptores [01]
    (124, 17,  'withholding_base',              Numeric),  # Base retenciones e ingresos a cuenta [02]
    (141, 17,  'withholding_tax',               Numeric(sign=WITHOUT_SIGN)),  # Retenciones e ingresos a cuenta [03]
    (158, 17,  'previous_declaration_result',   Numeric),  # Resultado anteriores declaraciones [04]
    (175, 17,  'total_of_declaration',          Numeric),  # Resultado a ingresar [03] - [04]

    (192, 1,   'complementary_autoliquidation', Boolean(BOOLEAN_X)),  # Declaración complementaria.
    (193, 13,  'previous_declaration_receipt',  Char),  # Número de justificante de la declaración anterior
    (206, 34,  'bank_account',                  Char),  # Domiciliación IBAN
    (240, 236, 'reserved_aeat',                 Char),  # Reservado AEAT
    (476, 13,  'electronic_stamp_aeat',         Char),  # Reservado para la Administración. Sello electronico
    (489, 12,  'record_end',                    Const(f'</T{MODEL}01000>')) # Identificador de fin de registro.
)

RECORDS = (RECORD, )

def write(header_record, record, footer_record):
    """
    Use this method to write as file this record design
    :param header_record: Record using HEADER_RECORD structure
    :param record: Record using RECORD structure
    :param footer_record: Record using FOOTER_RECORD structure
    :return: file
    """
    return header_record.write() + record.write() + footer_record.write()