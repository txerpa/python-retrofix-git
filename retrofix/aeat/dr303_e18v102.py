# -*- coding: utf-8 -*-

from retrofix.fields import *
from retrofix.record import Record

"""
Diseño de registro del modelo 303 (e 2018 v 1.02)
-----------------------------------------------------------------------------------------------------
303 - Orden HAP/2373/2014, de 9 de diciembre (ejercicio 2018 y siguientes) 

Régimen General. Declaración trimestral.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '303'

# DP30300
HEADER_RECORD = (
    (1,   2,   'open_tag',             Const('<T')),     # Constante.
    (3,   3,   'model',                Const(MODEL)),    # Modelo
    (6,   1,   'page',                 Const('0')),      # Discriminante.
    (7,   4,   'fiscalyear',           Number),          # Ejercicio devengo. (AAAA)
    (11,  2,   'period',               Char),            # Período. (PP)
    (13,  5,   'close_tag',            Const('0000>')),  # Tipo y cierre
    (18,  5,   'open_aux',             Const('<AUX>')),  # Constante
    (23,  70,  'aeat_1',               Char),            # Reservado para la Administración. Rellenar con blancos
    (93,  4,   'program_version',      Char),            # Versión del Programa (Nota 1)
    (97,  4,   'aeat_2',               Char),            # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat',      Char),            # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',               Char),            # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',            Const('</AUX>'))  # Constante
)

FOOTER_RECORD = (
    (1, 3, 'open_tag',                 Const('</T')),   # Constante.
    (4, 3, 'model',                    Const(MODEL)),   # Modelo
    (7, 1, 'page',                     Const('0')),     # Constante.
    (8, 4, 'fiscalyear',               Number),         # Ejercicio devengo. (AAAA)
    (12, 2, 'period',                  Char),           # Período. (PP)
    (14, 5, 'close_tag',               Const('0000>')), # Constante
    (19, 2, 'record_end',              Const('\r\n'))   # Fin de Registro. Constante CRLF( Hexadecimal 0D0A, Decimal 1310)
)

# DP30301
PAGE1_RECORD = (
    (1,    2,	'model_start',                                          Const('<T')),  # Inicio del identificador de modelo y página.
    (3,    3,	'model',                                                Const(MODEL)),  # Modelo.
    (6,    5,	'page',                                                 Const('01000')),  # Página.
    (11,   1,	'model_end',                                            Const('>')),  # Fin de identificador de modelo.
    (12,   1,	'indicator_complementary_page',                         Char),  # Indicador de página complementaria.
    (13,   1,	'declaration_type',                                     Char),  # Tipo Declaración
    # Identificación (1)
    (14,   9,	'nif',                                                  Char),  # NIF
    (23,   60,	'company_name',                                         Char),  # Apellidos o Razón Social
    (83,   20,	'first_name',                                           Char),  # Nombre
    # Devengo (2)
    (103,  4,	'fiscalyear',                                           Number),  # Ejercicio
    (107,  2,	'period',                                               Char),  # Período
    # Identificación (1)
    (109,  1,	'monthly_return_subscription',                          Boolean(BOOLEAN_12)),  # Inscrito en el Registro de devolución mensual (Art. 30 RIVA)
    (110,  1,	'simplificated_regime',                                 Number),  # Tributa exclusivamente en Régimen Simplificado (RS)
    (111,  1,	'join_liquidation',                                     Boolean(BOOLEAN_12)),  # Autoliquidación conjunta
    (112,  1,	'bankruptcy',                                           Boolean(BOOLEAN_12)),  # Declarado en concurso de acreedores en el presente período de liquidación
    (113,  8,	'auto_bankruptcy_date',                                 Date('%Y%m%d')),  # Fecha en que se dictó el auto de declaración de concurso
    (121,  1,	'auto_bankruptcy_declaration',                          Char),  # Auto de declaración de concurso dictado en el período
    (122,  1,	'recc',                                                 Boolean(BOOLEAN_12)),  # Opción por el régimen especial de criterio de Caja
    (123,  1,	'recc_receiver',                                        Boolean(BOOLEAN_12)),  # Destinatario de las operaciones a las que se aplique el régimen especial del criterio de Caja
    (124,  1,	'special_prorate',                                      Boolean(BOOLEAN_12)),  # Opción por la aplicación de la prorrata especial
    (125,  1,	'special_prorate_revocation',                           Boolean(BOOLEAN_12)),  # Revocación de la opción por la aplicación de la prorrata especial
    # Liquidación (3) - Regimen General - IVA Devengado - Régimen general 
    (126,  17,	'accrued_vat_base_1',                                   Numeric(sign=WITHOUT_SIGN)),  # [01] Base imponible
    (143,  5,	'accrued_vat_percent_1',                                Numeric(sign=WITHOUT_SIGN)),  # [02] Tipo %
    (148,  17,	'accrued_vat_tax_1',                                    Numeric(sign=WITHOUT_SIGN)),  # [03] Cuota
    (165,  17,	'accrued_vat_base_2',                                   Numeric(sign=WITHOUT_SIGN)),  # [04] Base imponible
    (182,  5,	'accrued_vat_percent_2',                                Numeric(sign=WITHOUT_SIGN)),  # [05] Tipo %
    (187,  17,	'accrued_vat_tax_2',                                    Numeric(sign=WITHOUT_SIGN)),  # [06] Cuota
    (204,  17,	'accrued_vat_base_3',                                   Numeric(sign=WITHOUT_SIGN)),  # [07] Base imponible
    (221,  5,	'accrued_vat_percent_3',                                Numeric(sign=WITHOUT_SIGN)),  # [08] Tipo %
    (226,  17,	'accrued_vat_tax_3',                                    Numeric(sign=WITHOUT_SIGN)),  # [09] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Adquisiciones intracomunitarias de bienes y servicios 
    (243,  17,	'intracommunity_adquisitions_base',                     Numeric(sign=WITHOUT_SIGN)),  # [10] Base imponible
    (260,  17,	'intracommunity_adquisitions_tax',                      Numeric(sign=WITHOUT_SIGN)),  # [11] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Otras operaciones con inversión del sujeto pasivo (excepto. adq. intracom) 
    (277,  17,	'other_passive_subject_base',                           Numeric(sign=WITHOUT_SIGN)),  # [12] Base imponible
    (294,  17,	'other_passive_subject_tax',                            Numeric(sign=WITHOUT_SIGN)),  # [13] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Modificación bases y cuotas
    (311,  17,	'accrued_vat_base_modification',                        Numeric(sign=SIGN_N)),  # [14] Base imponible
    (328,  17,	'accrued_vat_tax_modification',                         Numeric(sign=SIGN_N)),  # [15] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Recargo equivalencia
    (345,  17,	'accrued_re_base_1',                                    Numeric(sign=WITHOUT_SIGN)),  # [16] Base imponible
    (362,  5,	'accrued_re_percent_1',                                 Numeric(sign=WITHOUT_SIGN)),  # [17] Tipo %
    (367,  17,	'accrued_re_tax_1',                                     Numeric(sign=WITHOUT_SIGN)),  # [18] Cuota
    (384,  17,	'accrued_re_base_2',                                    Numeric(sign=WITHOUT_SIGN)),  # [19] Base imponible
    (401,  5,	'accrued_re_percent_2',                                 Numeric(sign=WITHOUT_SIGN)),  # [20] Tipo %
    (406,  17,	'accrued_re_tax_2',                                     Numeric(sign=WITHOUT_SIGN)),  # [21] Cuota
    (423,  17,	'accrued_re_base_3',                                    Numeric(sign=WITHOUT_SIGN)),  # [22] Base imponible
    (440,  5,	'accrued_re_percent_3',                                 Numeric(sign=WITHOUT_SIGN)),  # [23] Tipo %
    (445,  17,	'accrued_re_tax_3',                                     Numeric(sign=WITHOUT_SIGN)),  # [24] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado - Modificaciones bases y cuotas del recargo de equivalencia
    (462,  17,	'accrued_re_base_modification',                         Numeric(sign=SIGN_N)),  # [25] Base imponible
    (479,  17,	'accrued_re_tax_modification',                          Numeric(sign=SIGN_N)),  # [26] Cuota
    # Liquidación (3) - Regimen General - IVA Devengado
    (496,  17,	'accrued_total_tax',                                    Numeric(sign=SIGN_N)),  # [27] Total cuota devengada ( [03] + [06] + [09] + [11] + [13] + [15] + [18] + [21] + [24] + [26])
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en operaciones interiores corrientes
    (513,  17,	'deductible_current_domestic_operations_base',          Numeric(sign=WITHOUT_SIGN)),  # [28] Base
    (530,  17,	'deductible_current_domestic_operations_tax',           Numeric(sign=WITHOUT_SIGN)),  # [29] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en operaciones interiores con bienes de inversión
    (547,  17,	'deductible_investment_domestic_operations_base',       Numeric(sign=WITHOUT_SIGN)),  # [30] Base
    (564,  17,	'deductible_investment_domestic_operations_tax',        Numeric(sign=WITHOUT_SIGN)),  # [31] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en las importaciones de bienes corrientes
    (581,  17,	'deductible_current_import_operations_base',            Numeric(sign=WITHOUT_SIGN)),  # [32] Base
    (598,  17,	'deductible_current_import_operations_tax',             Numeric(sign=WITHOUT_SIGN)),  # [33] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Por cuotas soportadas en las importaciones de bienes de inversión
    (615,  17,	'deductible_investment_import_operations_base',         Numeric(sign=WITHOUT_SIGN)),  # [34] Base
    (632,  17,	'deductible_investment_import_operations_tax',          Numeric(sign=WITHOUT_SIGN)),  # [35] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - En adquisiciones intracomunitarias de bienes y servicios corrientes
    (649,  17,	'deductible_current_intracommunity_operations_base',    Numeric(sign=WITHOUT_SIGN)),  # [36] Base
    (666,  17,	'deductible_current_intracommunity_operations_tax',     Numeric(sign=WITHOUT_SIGN)),  # [37] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - En adquisiciones intracomunitarias de bienes de inversión
    (683,  17,	'deductible_investment_intracommunity_operations_base', Numeric(sign=WITHOUT_SIGN)),  # [38] Base
    (700,  17,	'deductible_investment_intracommunity_operations_tax',  Numeric(sign=WITHOUT_SIGN)),  # [39] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible - Rectificación de deducciones
    (717,  17,	'deductible_regularization_base',                       Numeric(sign=SIGN_N)),  # [40] Base
    (734,  17,	'deductible_regularization_tax',                        Numeric(sign=SIGN_N)),  # [41] Cuota
    # Liquidación (3) - Regimen General - IVA Deducible
    (751,  17,	'deductible_compensations',                             Numeric(sign=SIGN_N)),  # [42] Compensaciones Régimen Especial A.G. y P. - Cuota
    (768,  17,	'deductible_investment_regularization',                 Numeric(sign=SIGN_N)),  # [43] Regularización inversiones - Cuota
    (785,  17,	'deductible_pro_rata_regularization',                   Numeric(sign=SIGN_N)),  # [44] Regularización por aplicación del porcentaje definitivo de prorrata - Cuota
    (802,  17,	'deductible_total',                                     Numeric(sign=SIGN_N)),  # [45] Total a deducir ( [29] + [31] + [33] + [35] + [37] + [39] + [41] + [42] + [43] + [44] ) - Cuota
    (819,  17,	'difference',                                           Numeric(sign=SIGN_N)),  # [46] Resultado régimen general ( [27] - [45] ) - Cuota
    # Identificación (1) 
    (836,  1,   'has_operations',                                       Number),  # ¿Existe volumen de operaciones (art. 121 LIVA)?
    (837,  1,   'foral_tax_admin',                                      Number),  # Sujeto pasivo que tributa exclusivamente a una Administración tributaria Foral con IVA a la importación liquidado por la Aduana pendiente de ingreso
    (838,  1,   'vat_book_voluntary',                                   Number),  # ¿Ha llevado voluntariamente los Libros registro del IVA a través de la Sede electrónica de la AEAT durante el ejercicio?
    (839,  1,   'exonerated_m390',                                      Number),  # ¿Está exonerado de la Declaración-resumen anual del IVA, modelo 390?

    (840,  578,	'reserved_aeat',                                        Char),  # Reservado para la AEAT
    (1418, 13,	'electronic_stamp_aeat',                                Char),  # Reservado para la AEAT - Sello electrónico reservado para la AEAT
    (1431, 12,  'record_end_id',                                        Const(f'</T{MODEL}01000>')),# Indicador de fin de registro
)

# DP30302
PAGE2_RECORD = ()

# DP30303
PAGE3_RECORD = (
    (1,	   2,  'model_start',                                                      Const('<T')),  # Inicio del identificador de modelo y página.
    (3,	   3,  'model',                                                            Const('303')),  # Modelo.
    (6,	   5,  'page',                                                             Const('03000')),  # Página.
    (11,   1,  'model_end',                                                        Const('>')),  # Fin de identificador de modelo.
    # Información adicional 
    (12,   17, 'intracommunity_deliveries',                                        Numeric(sign=SIGN_N)),  # [59] Entregas intracomunitarias de bienes y servicios
    (29,   17, 'exports',                                                          Numeric(sign=SIGN_N)),  # [60] Exportaciones y operaciones asimiladas
    (46,   17, 'not_subject_or_reverse_charge',                                    Numeric(sign=SIGN_N)),  # [61] Operaciones no sujetas o con inversión del sujeto pasivo que originan el derecho a deducción
    (63,   17, 'recc_deliveries_base',                                             Numeric(sign=SIGN_N)),  # [62] Importes devengados en período de liquidación según art. 75 LIVA. - Base Imponible
    (80,   17, 'recc_deliveries_tax',                                              Numeric(sign=SIGN_N)),  # [63] Importes devengados en período de liquidación según art. 75 LIVA. - Cuota
    (97,   17, 'recc_adquisitions_base',                                           Numeric(sign=SIGN_N)),  # [74] Cuotas de IVA soportados en operaciones que tributen por el régimen especial del criterio de caja conforme a la regla general de devengo contenida en el artículo 75 LIVA. - Base Imponible
    (114,  17, 'recc_adquisitions_tax',                                            Numeric(sign=SIGN_N)),  # [75] Cuotas totales de IVA soportados en operaciones que tributen por el régimen especial del criterio de caja conforme a la regla general de devengo contenida en el artículo 75 de LIVA. - Cuota
    # Resultado 
    (131,  17, 'tax_regularization',                                               Numeric(sign=SIGN_N)),  # [76] Regularización cuotas art. 80.cinco.5ª LIVA
    (148,  17, 'total_difference',                                                 Numeric(sign=SIGN_N)),  # [64] Suma de resultados ( [46] + [58] + [76] )
    (165,  5,  'state_administration_percent',                                     Numeric(sign=WITHOUT_SIGN)),  # [65] % Atribuible a la Administración del Estado
    (170,  4,  'aeat_1',                                                           Const('0000')),  # Reservado para la AEAT
    (174,  17, 'state_administration_amount',                                      Numeric(sign=SIGN_N)),  # [66] Atribuible a la Administración del Estado
    (191,  17, 'import_taxes',                                                     Numeric(sign=WITHOUT_SIGN)),  # [77] IVA a la importación liquidado por la Aduana pendiente de ingreso
    (208,  17, 'previous_period_amount_to_compensate',                             Numeric(sign=WITHOUT_SIGN)),  # [67] Cuotas a compensar de periodos anteriores
    (225,  17, 'joint_taxation_state_provincial_councils',                         Numeric(sign=SIGN_N)),  # [68] Exclusivamente para sujetos pasivos que tributan conjuntamente a la Administración del Estado y a las Diputaciones Forales Resultado de la regularización anual
    (242,  17, 'result',                                                           Numeric(sign=SIGN_N)),  # [69]Resultado ( [66] + [77] - [67] ± [68] )
    (259,  17, 'to_deduce',                                                        Numeric(sign=SIGN_N)),  # [70] A deducir
    (276,  17, 'liquidation_result',                                               Numeric(sign=SIGN_N)),  # [71] Resultado de la liquidación ( [69] - [70] )

    (293,  1,  'complementary_autoliquidation',                                    Boolean(BOOLEAN_X)),  # Declaración complementaria
    (294,  13, 'previous_declaration_receipt',                                     Char),  # Número justificante declaración anterior
    (307,  1,  'without_activity',                                                 Boolean(BOOLEAN_X)),  # Declaración Sin actividad
    (308,  11, 'swiftbic',                                                         Char),  # Devolución. SWIFT-BIC
    (319,  34, 'bank_account',                                                     Char),  # Domiciliación/Devolución - IBAN
    # Información adicional - Exclusivamente a cumplimentar en el último periodo exonerados de la Declaración-resumen anual del IVA
    (353,  1,  'additional_info_main_key',                                         Const('0')),  # B - Clave - Principal
    (354,  4,  'additional_info_main_epigraph',                                    Char),  # C - Epígrafe IAE - Principal
    (358,  1,  'additional_info_others_key_1',                                     Const('0')),  # B - Clave - Otras - 1ª
    (359,  4,  'additional_info_others_epigraph_1',                                Char),  # C - Epígrafe IAE - Otras - 1ª
    (363,  1,  'additional_info_others_key_2',                                     Const('0')),  # B - Clave - Otras - 2ª
    (364,  4,  'additional_info_others_epigraph_2',                                Char),  # C - Epígrafe IAE - Otras - 2ª
    (368,  1,  'additional_info_others_key_3',                                     Const('0')),  # B - Clave - Otras - 3ª
    (369,  4,  'additional_info_others_epigraph_3',                                Char),  # C - Epígrafe IAE - Otras - 3ª
    (373,  1,  'additional_info_others_key_4',                                     Const('0')),  # B - Clave - Otras - 4ª
    (374,  4,  'additional_info_others_epigraph_4',                                Char),  # C - Epígrafe IAE - Otras - 4ª
    (378,  1,  'additional_info_others_key_5',                                     Const('0')),  # B - Clave - Otras - 5ª
    (379,  4,  'additional_info_others_epigraph_5',                                Char),  # C - Epígrafe IAE - Otras - 5ª
    (383,  1,  'additional_info_need_to_present_third_parties_yearly_declaration', Boolean(BOOLEAN_X)),  # D - Marque si ha efectuado operaciones por las que tenga obligación de presentar la declaración anual de operaciones con terceras personas.
    # Operaciones realizadas en el ejercicio
    (384,  17, 'additional_info_general_operations',                               Numeric(sign=SIGN_N)),  # [80] Operaciones en régimen general
    (401,  17, 'additional_info_recc_special_operations',                          Numeric(sign=SIGN_N)),  # [81] Operaciones en régimen especial del criterio de caja conforme art. 75 LIVA
    (418,  17, 'additional_info_deductible_operations',                            Numeric(sign=SIGN_N)),  # [93] Entregas intracomunitarias exentas
    (435,  17, 'additional_info_not_deductible_operations',                        Numeric(sign=SIGN_N)),  # [83] Operaciones exentas sin derecho a deducción
    (452,  17, 'additional_info_not_location_rules_operations',                    Numeric(sign=SIGN_N)),  # [84] Operaciones no sujetas por reglas de localización o con inversión del sujeto pasivo
    (469,  17, 'additional_info_communitary_operations',                           Numeric(sign=SIGN_N)),  # [85] Entregas de bienes objeto de instalación o montaje en otros Estados miembros
    (486,  17, 'additional_info_special_operations',                               Numeric(sign=SIGN_N)),  # [86]Operaciones en régimen simplificado
    (503,  17, 'additional_info_not_normal_operations',                            Numeric(sign=SIGN_N)),  # [79] Entregas de bienes inmuebles y operaciones financieras no habituales
    (520,  17, 'additional_info_total',                                            Numeric(sign=SIGN_N)),  # [88] Total volumen de operaciones ([80]+[81]+[93]+[94]+[83]+[84]+[85]+[86]+[95]+[96]+[97]+[98]-[79]-[99])
    (537,  1,  'additional_info_anual_390',                                        Integer),  # Reservado para la AEAT
    (538,  5,  'additional_info_alava',                                            Numeric(sign=WITHOUT_SIGN)),  # [89] Información de la tributación por razón de territorio: Álava
    (543,  5,  'additional_info_guipuzcoa',                                        Numeric(sign=WITHOUT_SIGN)),  # [90] Información de la tributación por razón de territorio: Guipuzcoa
    (548,  5,  'additional_info_vizcaya',                                          Numeric(sign=WITHOUT_SIGN)),  # [91] Información de la tributación por razón de territorio: Vizcaya
    (553,  5,  'additional_info_navarra',                                          Numeric(sign=WITHOUT_SIGN)),  # [92] Información de la tributación por razón de territorio: Navarra
    # Exclusivamente a cumplimentar en el último periodo exonerados de la Declaración-resumen anual del IVA 
    (558,  17, 'additional_info_exempt_op_2bdeduced',                              Numeric(sign=SIGN_N)),  # [94] Exportaciones y otras operaciones exentas con derecho a deducción
    (575,  17, 'additional_info_farming_cattleraising_fishing',                    Numeric(sign=SIGN_N)),  # [95] Operaciones en régimen especial de la agricultura, ganadería y pesca
    (592,  17, 'additional_info_passive_subject_re',                               Numeric(sign=SIGN_N)),  # [96] Operaciones realizadas por sujetos pasivos acogidos al régimen especial del recargo de equivalencia
    (609,  17, 'additional_info_art_antiques_collectibles',                        Numeric(sign=SIGN_N)),  # [97] Operaciones en Régimen especial de bienes usados, objetos de arte, antigüedades y objetos de colección
    (626,  17, 'additional_info_travel_agency',                                    Numeric(sign=SIGN_N)),  # [98] Operaciones en régimen especial de Agencias de Viajes
    (643,  17, 'additional_info_delivery_investment_domestic_operations',          Numeric(sign=SIGN_N)),  # [99] Entregas de bienes de inversión
    (660,  5,  'field_107',                                                        Numeric(sign=WITHOUT_SIGN)), # [107] Información de la tributación por razón de territorio: Territorio común
    (665,  463, 'reserved_aeat',                                                  Char),  # Reservado para la AEAT
    (1128, 12,	'record_end_id',                                                   Const('</T30303000>')), # Indicador de fin de registro
)

# DP30304
PAGE4_RECORD = ()

RECORDS = (PAGE1_RECORD, PAGE2_RECORD, PAGE3_RECORD, PAGE4_RECORD, )


def write(header_record, footer_record, record_page1=None, record_page2=None, record_page3=None, record_page4=None):
    """
    Use this method to write as file this record design
    :param header_record: Record using HEADER_RECORD structure
    :param footer_record: Record using FOOTER_RECORD structure
    :param record_page1: Record using PAGE1_RECORD structure
    :param record_page2: Record using PAGE2_RECORD structure
    :param record_page3: Record using PAGE3_RECORD structure
    :param record_page4: Record using PAGE4_RECORD structure
    :return: file
    """
    file = header_record.write()
    if record_page1:
        file += record_page1.write()
    if record_page2:
        file += record_page2.write()
    if record_page3:
        file += record_page3.write()
    if record_page4:
        file += record_page4.write()
    file += footer_record.write()

    return file


def read(data):
    lines = data.splitlines()
    records = []
    current_line = lines.pop(0)
    records.append(Record.extract(current_line, PAGE1_RECORD))
    return records
