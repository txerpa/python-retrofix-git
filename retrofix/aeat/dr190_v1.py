# -*- coding: utf-8 -*-

from retrofix.fields import Const, WITHOUT_SIGN, Number, Char, Integer, Numeric, SIGN_N_BLANK

"""
Diseño de registro del modelo 190 (v 1)
-----------------------------------------------------------------------------------------------------
190 - Orden EHA/3127/2009 (actualizado por Orden HFP/1106/2017) 

Retenciones e ingresos a cuenta. 
Rendimientos del trabajo y de actividades económicas, premios y determinadas ganancias 
patrimoniales e imputaciones de rentas. Resumen anual.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '190'

# Tipo 1: Registro de declarante
RECORD_1 = (
    (1,   1,   'record_type',                                            Const('1')),  # TIPO DE REGISTRO.
    (2,   3,   'model',                                                  Const(MODEL)),  # MODELO DECLARACIÓN.
    (5,   4,   'fiscalyear',                                             Number),  # EJERCICIO.
    (9,   9,   'nif',                                                    Char),  # NIF DEL DECLARANTE
    (18,  40,  'presenter_name',                                         Char),  # APELLIDOS Y NOMBRE, DENOMINACIÓN O RAZÓN SOCIAL DEL DECLARANTE
    (58,  1,   'support_type',                                           Char),  # TIPO DE SOPORTE.
    # PERSONA CON QUIÉN RELACIONARSE
    (59,  9,   'contact_phone',                                          Number),  # PERSONA CON QUIÉN RELACIONARSE, TELÉFONO
    (68,  40,  'contact_name',                                           Char),  # PERSONA CON QUIÉN RELACIONARSE, APELLIDOS Y NOMBRE

    (108, 13,  'declaration_number',                                     Number),  # NÚMERO IDENTIFICATIVO DE LA DECLARACIÓN.
    # DECLARACION COMPLEMENTARIA O SUSTITUTIVA
    (121, 1,   'complementary',                                          Char),  # DECLARACIÓN COMPLEMENTARIA
    (122, 1,   'replacement',                                            Char),  # DECLARACIÓN SUSTITUTIVA

    (123, 13,  'previous_declaration_receipt',                           Number),  # NÚMERO IDENTIFICATIVO DE LA DECLARACIÓN ANTERIOR
    (136, 9,   'number_of_withholdings',                                 Integer),  # NÚMERO TOTAL DE PERCEPCIONES
    (145, 16,  'total_withholding_base',                                 Numeric(sign=SIGN_N_BLANK)),  # IMPORTE TOTAL DE LAS PERCEPCIONES.
    (161, 15,  'total_withholding_tax',                                  Numeric(sign=WITHOUT_SIGN)),  # IMPORTE TOTAL DE LAS RETENCIONES E INGRESOS A CUENTA.
    (176, 50,  'contact_email',                                          Char),  # PERSONA CON QUIÉN RELACIONARSE, CORREO ELECTRÓNICO
    (488, 13,  'electronic_stamp_aeat',                                  Char)                          # SELLO ELECTRÓNICO
)

# Tipo 2: Registro de perceptor
RECORD_2 = (
    (1,   1,   'record_type',                                            Const('2')),  # TIPO DE REGISTRO
    (2,   3,   'model',                                                  Const(MODEL)),  # MODELO DECLARACIÓN
    (5,   4,   'fiscalyear_receiver',                                    Number),  # EJERCICIO.
    (9,   9,   'declarant_nif',                                          Char),  # NIF DEL DECLARANTE
    (18,  9,   'receiver_nif',                                           Char),  # NIF DEL PERCEPTOR
    (27,  9,   'representative_nif',                                     Char),  # NIF DEL REPRESENTANTE LEGAL
    (36,  40,  'receiver_name',                                          Char),  # APELLIDOS Y NOMBRE O DENOMINACIÓN DEL PERCEPTOR
    (76,  2,   'province_code',                                          Number),  # CÓDIGO DE PROVINCIA
    (78,  1,   'withholding_key',                                        Char),  # CLAVE DE PERCEPCIÓN
    (79,  2,   'witholding_subkey',                                      Char),  # SUBCLAVE
    # PERCEPCIONES DINERARIAS NO DERIVADAS DE INCAPACIDAD LABORAL                          
    (81,  14,  'receiver_income_withholding_base',                       Numeric(sign=SIGN_N_BLANK)),  # PERCEPCIÓN ÍNTEGRA
    (95,  13,  'receiver_income_withholding_tax',                        Numeric(sign=WITHOUT_SIGN)),  # RETENCIONES PRACTICADAS
    # PERCEPCIONES EN ESPECIE NO DERIVADAS DE INCAPACIDAD LABORAL
    (108, 14,  'receiver_in_kind_withholding_base',                      Numeric(sign=SIGN_N_BLANK)),  # VALORACIÓN
    (122, 13,  'receiver_in_kind_withholding_tax_payed',                 Numeric(sign=WITHOUT_SIGN)),  # INGRESOS A CUENTA EFECTUADOS
    (135, 13,  'receiver_in_kind_withholding_tax_earned',                Numeric(sign=WITHOUT_SIGN)),  # INGRESOS A CUENTA REPERCUTIDOS

    (148, 4,   'other_fiscalyear',                                       Number),  # EJERCICIO DEVENGO
    (152, 1,   'ceuta_or_melilla',                                       Number),  # CEUTA O MELILLA
    # DATOS ADICIONALES (solo en las claves A, B.01, B.03, C, E.01 y E.02).
    (153, 4,   'birth_date_receiver',                                    Integer),  # AÑO DE NACIMIENTO
    (157, 1,   'marital_status_receiver',                                Integer),  # SITUACIÓN FAMILIAR
    (158, 9,   'spouse_nif_receiver',                                    Char),     # NIF DEL CÓNYUGE
    (167, 1,   'disability_receiver',                                    Integer),  # DISCAPACIDAD
    (168, 1,   'employment_relationship',                                Integer),  # CONTRATO O RELACIÓN
    (169, 1,   'titular_convivencia',                                    Const('0')),  # TITULAR UNIDAD DE CONVIVENCIA
    (170, 1,   'geographic_mobility',                                    Integer),  # MOVILIDAD GEOGRÁFICA
    (171, 13,  'reductions_applied',                                     Numeric(sign=WITHOUT_SIGN)),  # REDUCCIONES APLICABLES
    (184, 13,  'deductible_expenses',                                    Numeric(sign=WITHOUT_SIGN)),  # GASTOS DEDUCIBLES
    (197, 13,  'compensatory_maintenance',                               Numeric(sign=WITHOUT_SIGN)),  # PENSIONES COMPENSATORIAS
    (210, 13,  'food_annuity',                                           Numeric(sign=WITHOUT_SIGN)),  # ANUALIDADES POR ALIMENTOS
    # HIJOS Y OTROS DESCENDIENTES
    (223, 1,   'total_children_under_three',                             Integer),  # Nº TOTAL '< 3 AÑOS'
    (224, 1,   'computed_children_under_three',                          Integer),  # POR ENTERO '< 3 AÑOS'
    (225, 2,   'total_other_children',                                   Integer),  # Nº TOTAL 'RESTO'
    (227, 2,   'other_children_computed',                                Integer),  # POR ENTERO 'RESTO'
    (229, 2,   'total_children_disability_between_33_65',                Integer),  # Nº TOTAL '>= 33% Y < 65%'
    (231, 2,   'computed_children_disability_between_33_65',             Integer),  # POR ENTERO '>= 33% Y < 65%'
    # HIJOS Y OTROS DESCENDIENTES CON DISCAPACIDAD
    (233, 2,   'total_children_with_reduced_mobility',                   Integer),  # Nº TOTAL 'MOVILIDAD REDUCIDA'
    (235, 2,   'computed_children_with_reduced_mobility',                Integer),  # POR ENTERO 'MOVILIDAD REDUCIDA'
    (237, 2,   'total_children_disability_more_65',                      Integer),  # Nº TOTAL  '>= 65%'
    (239, 2,   'computed_children_disability_more_65',                   Integer),  # POR ENTERO  '>= 65%'
    # ASCENDIENTES
    (241, 1,   'total_ascendants_under_75',                              Integer),  # Nº TOTAL '< 75'
    (242, 1,   'computed_ascendants_under_75',                           Integer),  # POR ENTERO '< 75'
    (243, 1,   'total_ascendants_other',                                 Integer),  # Nº TOTAL '>= 75'
    (244, 1,   'computed_ascendants_other',                              Integer),  # POR ENTER '>= 75'
    # ASCENCIENTES CON DISCAPACIDAD
    (245, 1,   'total_ascendants_with_disability_between_33_65',         Integer),  # Nº TOTAL '>= 33% y < 65%'
    (246, 1,   'computed_ascendants_with_disability_between_33_65',      Integer),  # POR ENTERO '>= 33% y < 65%'
    (247, 1,   'total_ascendants_with_reduced_mobility',                 Integer),  # Nº TOTAL 'MOVILIDAD REDUCIDA'
    (248, 1,   'computed_ascendants_with_reduced_mobility',              Integer),  # POR ENTERO 'MOVILIDAD REDUCIDA'
    (249, 1,   'total_ascendants_with_disability_more_65',               Integer),  # Nº TOTAL '>= 65'
    (250, 1,   'computed_ascendants_with_disability_more_65',            Integer),  # POR ENTERO '>= 65'
    # CÓMPUTO DE LOS 3 PRIMEROS HIJOS
    (251, 1,   'first_child',                                            Integer),  # 1ER HIJO
    (252, 1,   'second_child',                                           Integer),  # 2º HIJO
    (253, 1,   'third_child',                                            Integer),  # 3ER HIJO

    (254, 1,   'home_mortgage_deduction',                                Integer),  # COMUNICACIÓN PRÉSTAMOS VIVIENDA HABITUAL
    # PERCEPCIONES DINERARIAS DERIVADAS DE INCAPACIDAD LABORAL
    (255, 14,  'receiver_income_withholding_base_lab_inhability',        Numeric(sign=SIGN_N_BLANK)),  # PERCEPCIÓN ÍNTEGRA
    (269, 13,  'receiver_income_withholding_tax_lab_inhability',         Numeric(sign=WITHOUT_SIGN)),  # RETENCIONES PRACTICADAS
    (282, 14,  'receiver_in_kind_withholding_base_lab_inhability',       Numeric(sign=SIGN_N_BLANK)),  # VALORACIÓN
    (296, 13,  'receiver_in_kind_withholding_tax_payed_lab_inhability',  Numeric(sign=WITHOUT_SIGN)),  # INGRESOS A CUENTA EFECTUADOS
    (309, 13,  'receiver_in_kind_withholding_tax_earned_lab_inhability', Numeric(sign=WITHOUT_SIGN)),  # INGRESOS A CUENTA REPERCUTIDOS
    (322, 1,   'complemento_ayuda_infancia',                             Const('0')),                  # COMPLEMENTO AYUDA PARA LA INFANCIA    
    (323, 178, 'blank',                                                  Char)                          # BLANCOS
)

RECORDS = (RECORD_1, RECORD_2, )

def write(record_1, records_2):
    """
    Use this method to write as file this record design
    :param record_1: Declarant record
    :param records_2: Receiver records List
    :return: file
    """
    file = record_1.write() + '\r\n'
    for record_2 in records_2:
        file += record_2.write() + '\r\n'
    return file