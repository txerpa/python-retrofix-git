# -*- coding: utf-8 -*-

from retrofix.fields import Const, Boolean, Char, Number, Numeric, BOOLEAN_X, SIGN_N, WITHOUT_SIGN

"""
Diseño de registro del modelo 130 (v 1.1)
-----------------------------------------------------------------------------------------------------
130 - Orden HAP/258/2015 (Ejercicios 2015 y siguientes)
  
Pago fraccionado. Empresarios y profesionales en Estimación Directa. Declaración – Liquidación.
-----------------------------------------------------------------------------------------------------
"""

MODEL = '130'

# DR 13000
HEADER_RECORD = (
    (1,   2,   'open_tag',                        Const('<T')),         # Constante.
    (3,   3,   'model',                           Const(MODEL)),        # Modelo
    (6,   1,   'page',                            Const('0')),          # Constante.
    (7,   4,   'fiscalyear',                      Number),              # Ejercicio devengo. (AAAA)
    (11,  2,   'period',                          Char),                # Período. (PP)
    (13,  5,   'close_tag',                       Const('0000>')),      # Constante
    (18,  5,   'open_aux',                        Const('<AUX>')),      # Constante
    (23,  70,  'aeat_1',                          Char),                # Reservado para la Administración. Rellenar con blancos
    (93,  4,   'program_version',                 Char),                # Versión del Programa (Nota 1)
    (97,  4,   'aeat_2',                          Char),                # Reservado para la Administración. Rellenar con blancos
    (101, 9,   'dev_company_vat',                 Char),                # NIF Empresa Desarrollo (Nota 1)
    (110, 213, 'aeat_3',                          Char),                # Reservado para la Administración. Rellenar con blancos
    (323, 6,   'close_aux',                       Const('</AUX>'))      # Constante
)                                                                       
                                                                        
FOOTER_RECORD = (                                                       
    (1, 3, 'open_tag',                            Const('</T')),        # Constante.
    (4, 3, 'model',                               Const(MODEL)),        # Modelo
    (7, 1, 'page',                                Const('0')),          # Constante.
    (8, 4, 'fiscalyear',                          Number),              # Ejercicio devengo. (AAAA)
    (12, 2, 'period',                             Char),                # Período. (PP)
    (14, 5, 'close_tag',                          Const('0000>')),      # Constante
    (19, 2, 'record_end',                         Const('\r\n'))        # Fin de Registro. Constante CRLF( Hexadecimal 0D0A, Decimal 1310)
)

# DR 13001
RECORD = (
    (1,	  2,  'model_start',                      Const('<T')),  # Inicio del identificador de modelo y página
    (3,	  3,  'model',                            Const(MODEL)),  # Modelo
    (6,	  2,  'page',                             Const('01')),  # Página
    (8,	  4,  'model_end',                        Const('000>')),  # Fin de identificador de modelo
    (12,  1,  'complementary_page',               Const(' ')),  # Indicador de página complementaria
    (13,  1,  'declaration_type',                 Char),  # Tipo de declaración
    # Declarante (1)
    (14,  9,  'nif',                              Char),  # Sujeto pasivo. NIF
    (23,  60, 'surname',                          Char),  # Sujeto pasivo. Apellidos
    (83,  20, 'first_name',                       Char),  # Sujeto pasivo. Nombre
    # Devengo (2)
    (103, 4,  'fiscalyear',                       Number),  # Ejercicio
    (107, 2,  'period',                           Char),  # Periodo
    # Liquidación (3)
    # I. Actividades econ. Estim. Directa
    (109, 17, 'period_income',                    Numeric),  # [01] Ingresos computables correspondientes al conjunto …
    (126, 17, 'period_expenses',                  Numeric),  # [02] Gastos fiscalmente deducibles (…)
    (143, 17, 'net_profit',                       Numeric(sign=SIGN_N)),  # [03]Rendimiento neto ([01] - [02]).
    (160, 17, 'net_profit_020',                   Numeric),  # [04] 20 por 100 del importe de la casilla [03].
    (177, 17, 'to_deduce_of_last_periods',        Numeric),  # [05] A deducir : De los trim. anteriores, suma de los importes (…)
    (194, 17, 'already_supported',                Numeric(sign=WITHOUT_SIGN)),  # [06] A deducir: Retenciones e ingresos a cta. soportados (…)
    (211, 17, 'to_deduce_result',                 Numeric(sign=SIGN_N)),  # [07] Pago fraccionado previo del trimestre ([04]-[05]-[06])
    # II. Actividades agrícolas, ganaderas, etc. 
    (228, 17, 'ag_activities_income',             Numeric),  # [08] Volumen de ingresos del trimestre (…)
    (245, 17, 'ag_activities_income_020',         Numeric),  # [09] 2 por 100 del importe de la casilla [08]
    (262, 17, 'ag_activities_already_supported',  Numeric),  # [10] A deducir: Retenciones e Ingresos a cuenta (…)
    (279, 17, 'ag_activities_to_deduce_result',   Numeric(sign=SIGN_N)),  # [11] Pago fraccionado previo del trimestre (…) ([09]-[10])
    # III. Total Liquidación 
    (296, 17, 'sum_of_results',                   Numeric),  # [12] Suma de pagos fraccionados del trimestre ([07]+[11])
    (313, 17, 'activities_yield',                 Numeric),  # [13] A deducir: Minorac. por aplic. de la deducc. art. 110,3 del Reglam. del Impto.
    (330, 17, 'difference',                       Numeric(sign=SIGN_N)),  # [14] Diferencia ([12]-[13])
    (347, 17, 'previous_negative_results',        Numeric),  # [15] A deducir: Resultados negativos ejercicios anteriores
    (364, 17, 'home_mortgage_deduction',          Numeric),  # [16] A deducir: cantidades al pago adquis. o rehab. vivienda habitual (…)
    (381, 17, 'total_sums',                       Numeric(sign=SIGN_N)),  # [17] Total ([14]-[15]-[16])
    (398, 17, 'complementary_declaration_result', Numeric),  # [18] A deducir (exclus. complementaria) Resultado anteriores liquidaciones (…)
    (415, 17, 'total_of_declaration',             Numeric(sign=SIGN_N)),  # [19] Resultado de la autoliquidación ([17]-[18])

    (432, 1,  'complementary_autoliquidation',    Boolean(BOOLEAN_X)),  # Declaración complementaria.
    (433, 13, 'previous_declaration_receipt',     Char),  # Numero de justificante de la declaración anterior
    (446, 34, 'bank_account',                     Char),  # Domiciliación IBAN
    (480, 96, 'reserved_aeat',                    Char),  # Reservado AEAT
    (576, 13, 'electronic_stamp_aeat',            Char),  # Reservado para la Administración. Sello electronico
    (589, 12, 'record_end_id',                    Const(f'</T{MODEL}01000>')) # Identificador de fin de registro.
)

RECORDS = (RECORD, )

def write(header_record, record, footer_record):
    """
    Use this method to write as file this record design
    :param header_record: Record using HEADER_RECORD structure
    :param record: Record using RECORD structure
    :param footer_record: Record using FOOTER_RECORD structure
    :return: file
    """
    return header_record.write() + record.write() + footer_record.write()